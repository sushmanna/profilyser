module.exports = function (app) {
    //user api
    var users = require('../controllers/users');
    app.post('/api/forgotpassword', users.forgotpassword);
    app.put('/api/profile', users.updateUser);
    app.get('/api/profile/:id', users.getUser);
    app.post('/api/sendmail', users.sendmail);

    var settings = require('../controllers/settings');
    app.put('/api/settings', settings.updateSettings);
    app.get('/api/settings/:id', settings.getSettings);
    app.get('/api/crawlerDay/:id', settings.getCrawlerDay);
    app.put('/api/crawlerDay/:id', settings.updateCrawlerDay);
    app.delete('/api/deletecrawlerdata/:userId', settings.deleteCrawlerData);
    app.put('/api/crawleraction/:userId', settings.crawlerAction);
    //app.put('/api/crawlerstatus/:userId', settings.crawlerStatus);

    var cms = require('../controllers/cms');
    app.get('/api/cmslist/:id', cms.listCms);
    app.get('/api/cms/:userId/:cmsId', cms.getCms);
    app.get('/api/cmscontent/:cmsKey', cms.getContent);
    app.put('/api/cms/:userId/:cmsId', cms.updateCms);

    var city = require('../controllers/city');
    app.get('/api/citylist/', city.listCities);
    app.get('/api/getcity/:cityId', city.getCity);
    app.post('/api/managecity/', city.createCity);
    app.put('/api/managecity/:cityId', city.editCity);
    app.delete('/api/managecity/:cityId', city.deleteCity);
    app.put('/api/defaultcity/:cityId', city.defaultCity);
    app.put('/api/crawlercitystatus/:cityId', city.changeCrawlerCityStatus);
    app.put('/api/crawlercityaccess/:cityId', city.changeCrawlerCityAccess);
    app.get('/api/getcitybyquery', city.getCityByQuery);


    var twitter = require('../controllers/twitter');
    app.get('/api/test/', twitter.sampleapi);
    //app.get('/api/test1/', twitter.sampleapibyclient); 
    app.get('/api/test1/', twitter.sampleapibyclient1);
    app.get('/api/getfamousprofiles', twitter.getfamousprofiles);
    app.get('/api/getbalancedprofiles', twitter.getbalancedprofiles);
    app.get('/api/getpromisingprofiles', twitter.getpromisingprofiles);
    app.get('/api/gettweets', twitter.gettweets);
    app.get('/api/getmaptweets', twitter.getmaptweets);
    //app.get('/api/searchtweets', twitter.searchtweets);

};
