var city = require('../models/city');
var $q = require('q');

var mongoose = require('mongoose');
var CityModel = mongoose.model('City');

exports.listCities = function (req, res) {
    CityModel.find({}, function (err1, setting) {
        if (err1)
        {
            res.status(202).json('No city Found');
        }

        res.send(setting);
    });
};
exports.getCity = function (req, res) {
    var id = req.params.cityId;
    var query = CityModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {
        if (result == null)
        {
            res.status(202).json('Invalid Id.');
        } else
        {
            var citydetails = {
                _id: result._id,
                name: result.name,
                west_longitude: result.west_longitude,
                south_latitude: result.south_latitude,
                east_longitude: result.east_longitude,
                north_latitude: result.north_latitude,
                consumer_key: result.consumer_key,
                consumer_secret: result.consumer_secret,
                access_token_key: result.access_token_key,
                access_token_secret: result.access_token_secret
            };
            res.json(citydetails);
        }
    });
};
exports.createCity = function (req, res) {


//    validation
    var validation = [];
    validation[0] = CityModel.count().where('consumer_key').equals(req.body.consumer_key).exec();
    validation[1] = CityModel.count().where('consumer_secret').equals(req.body.consumer_secret).exec();
    validation[2] = CityModel.count().where('access_token_key').equals(req.body.access_token_key).exec();
    validation[3] = CityModel.count().where('access_token_secret').equals(req.body.access_token_secret).exec();
    $q.all(validation).then(function (result) {
        console.log(result);
        var valid = true;
        for (x in result)
        {
            if (result[x] >= 2)
            {
                valid = false;
            }
        }
        if (valid)
        {
            var CityData = new CityModel(
                    {
                        name: req.body.name,
                        west_longitude: req.body.west_longitude,
                        south_latitude: req.body.south_latitude,
                        east_longitude: req.body.east_longitude,
                        north_latitude: req.body.north_latitude,
                        consumer_key: req.body.consumer_key,
                        consumer_secret: req.body.consumer_secret,
                        access_token_key: req.body.access_token_key,
                        access_token_secret: req.body.access_token_secret
                    });
            CityData.save(function (err, CityData) {
                if (err)
                {
                    res.status(202).json(err.message);
                }
                res.json(CityData);
            });
        }
        else
        {
            res.status(202).json('validation fail due to twitter api please check the api.');
        }
    });
};
exports.editCity = function (req, res) {
    // validation
    var validation = [];
    validation[0] = CityModel.count().where('_id').ne(req.params.cityId).where('consumer_key').equals(req.body.consumer_key).exec();
    validation[1] = CityModel.count().where('_id').ne(req.params.cityId).where('consumer_secret').equals(req.body.consumer_secret).exec();
    validation[2] = CityModel.count().where('_id').ne(req.params.cityId).where('access_token_key').equals(req.body.access_token_key).exec();
    validation[3] = CityModel.count().where('_id').ne(req.params.cityId).where('access_token_secret').equals(req.body.access_token_secret).exec();
    $q.all(validation).then(function (result) {
        var valid = true;
        for (x in result)
        {
            if (result[x] > 1)
            {
                valid = false;
            }
        }
        
        if (valid)
        {
            var id = req.params.cityId;
            var query = CityModel.findOne({});
            query.where('_id').equals(id);
            query.exec(function (err, result) {
                if (result == null)
                {
                    res.status(202).json('Invalid Id.');
                } else
                {
                    var citydetails = {
                        name: req.body.name,
                        west_longitude: req.body.west_longitude,
                        south_latitude: req.body.south_latitude,
                        east_longitude: req.body.east_longitude,
                        north_latitude: req.body.north_latitude,
                        consumer_key: req.body.consumer_key,
                        consumer_secret: req.body.consumer_secret,
                        access_token_key: req.body.access_token_key,
                        access_token_secret: req.body.access_token_secret
                    };
                    CityModel.findByIdAndUpdate(id, {$set: citydetails}, {'new': true}, function (err, data) {
                        if (data == null)
                        {
                            res.status(202).json(err.message);
                        } else
                        {
                            res.send('City Saved Successfully');
                        }
                    });
                }
            });
        }
        else
        {
            res.status(202).json('validation fail due to twitter api please check the api.');
        }
    });
};
exports.deleteCity = function (req, res) {
    var cityId = req.params.cityId;
    CityModel.remove({_id: cityId}, function (err) {
        if (!err) {
            CityModel.find({}, function (err1, setting) {
                res.send(setting);
            });
        }
        else {
            res.status(202).json('Deletion Failure');
        }
    });
};
exports.defaultCity = function (req, res) {
    var cityId = req.params.cityId;
    //change the default city;
    var conditions = {isDefault: true}
    , update = {$set: {isDefault: false}}
    , options = {multi: true};

    CityModel.update(conditions, update, options, callback);

    function callback(err, numAffected) {
        if (err)
        {
            res.status(202).json(err.message);
        } else
        {
            var conditions = {_id: cityId},
            update = {$set: {isDefault: true}},
            options = {multi: true};
            CityModel.update(conditions, update, options, callback1);
        }

        function callback1(err1, numAffected) {
            if (err)
            {
                res.status(202).json(err1.message);
            } else
            {
                CityModel.find({}, function (err1, setting) {
                    if (err1)
                    {
                        res.status(202).json('No city Found');
                    }

                    res.send(setting);
                });
            }
        }
    }
};
exports.changeCrawlerCityStatus = function (req, res) {
    var validation = [];
    validation[0] = CityModel.count().where('isCrawler').equals(true).exec();
    $q.all(validation).then(function (result) {
        var valid = true;
        if (req.body.status == true)
        {
            if (result[0] >= 10)
            {
                valid = false;
            }
        }
        if (valid)
        {
            var cityId = req.params.cityId;
            var updateData = {isCrawler: req.body.status};
            CityModel.findByIdAndUpdate(cityId, {$set: updateData}, {'new': true}, function (err, data) {
                if (data == null)
                {
                    res.status(202).json(err.message);
                } else
                {
                    //temp block this code
                    var exec1 = require('child_process').exec;
                    exec1('forever restart crawler-production.js -monitor -production', function (err, stdout, stderr) {
//                        console.log(stderr);
//                        console.log(stdout);
//                        console.log(err);
                    });
                    CityModel.find({}, function (err1, city) {
                        if (err1)
                        {
                            res.status(202).json('No city Found');
                        }

                        res.send(city);
                    });
                }
            });
        }
        else
        {
            res.status(202).json('Maximum 10 city are allowed.');
        }
    });
};


exports.changeCrawlerCityAccess = function (req, res) {
    var validation = [];
    validation[0] = CityModel.count().where('allowFreeAccess').equals(true).exec();
    $q.all(validation).then(function (result) {
        var valid = true;
        if (req.body.status == true)
        {
            if (result[0] >= 10)
            {
                valid = false;
            }
        }
        if (valid)
        {
            var cityId = req.params.cityId;
            var updateData = {allowFreeAccess: req.body.status};
            CityModel.findByIdAndUpdate(cityId, {$set: updateData}, {'new': true}, function (err, data) {
                if (data == null)
                {
                    res.status(202).json(err.message);
                } else
                {
                    CityModel.find({}, function (err1, city) {
                        if (err1)
                        {
                            res.status(202).json('No city Found');
                        }

                        res.send(city);
                    });
                }
            });
        }
        else
        {
            res.status(202).json('Maximum 10 city are allowed.');
        }
    });
};

exports.getCityByQuery = function(req, res) {
    var query = CityModel.find({});

    if( req.query.allow_free_access){
        query.where('allowFreeAccess').equals(req.query.allow_free_access);
        query.select('_id name');
    }

    if( req.query.to_city){
        query.where('name').equals(req.query.to_city);
        query.select('west_longitude south_latitude east_longitude north_latitude');
    }

    query.exec(function(err, result) {
        if (result == null) {
            res.status(202).json('Invalid credential.');
        } else {
            res.send(result);
        }
    });
};