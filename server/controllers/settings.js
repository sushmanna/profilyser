var setting = require('../models/setting');
var twitter = require('../models/tweet');
var $q = require('q');

var mongoose = require('mongoose');
var conn      = mongoose.createConnection('mongodb://localhost/sensor');
var conn2     = mongoose.createConnection('mongodb://localhost/crawler');


var SettingModel = conn.model('Setting');
var TwitterModel = conn2.model('Twitter');
var UserModel = conn.model('User');

exports.getSettings = function (req, res) {
    var id = req.params.id;
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {

        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            SettingModel.find({}, function (err1, setting) {
                res.send(setting);
            });
        } else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });
};
exports.getCrawlerDay = function (req, res) {
    var id = req.params.id;
    console.log(id);
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {
        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            var query1 = SettingModel.findOne({});
            query1.where('key').equals('auto_delete_crawler_day');
            query1.exec(function (err1, result1) {
                res.send(result1);
            });
        } else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });
};
exports.updateSettings = function (req, res) {

    var id = req.body.id;
    var promise;
    var settingPromises = [];
    //console.log(id);
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {

        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            //console.log(req.body.data);






            for (var t = 0; t < req.body.data.length; t++) {

                //set Time in seconds
                console.log(req.body.data[t]._id);
                var updateData = {
                    value: req.body.data[t].value
                };
                promise = SettingModel.findByIdAndUpdate(req.body.data[t]._id, {$set: updateData});
                settingPromises.push(promise);
            }
            $q.all(settingPromises).then(function (result) {
                console.log('All Setting Seved');
            }).then(function (tmpResult) {
                console.log('Return result');
                SettingModel.find({}, function (err1, setting) {
                    res.send(setting);
                });
            });
//            SettingModel.Promise
//                    .all(settingPromises)
//                    .then()
//                    .catch();
        }
        else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });

    //console.log(req.body);



};
exports.updateCrawlerDay = function (req, res) {
    var id = req.params.id;
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {
        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            var updateData = req.body;
            SettingModel.findByIdAndUpdate(req.body._id, {$set: updateData}, {'new': true}, function (err, data) {
                if (data == null)
                {
                    res.status(202).json('update failed');
                } else
                {
                    res.json(data);
                }
            });
        }
        else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });
};
exports.crawlerAction = function (req, res) {
    var id = req.params.userId;
    var action = req.body.action;
    var exec = require('child_process').exec;
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {
        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            exec(action, function (err, stdout, stderr) {
                console.log(stderr);
                console.log(stdout);
                console.log(err);
            });
            res.send('Action complete successfully');
        } else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });
};
//exports.crawlerStatus = function (req, res) {
//    var id = req.params.userId;
//    var action = req.body.action;
//    var exec = require('child_process').exec;
//    var query = UserModel.findOne({});
//    query.where('_id').equals(id);
//    query.exec(function (err, result) {
//
//        var access = result.roles.indexOf("admin");
//        if (access != -1)
//        {
//            exec('forever list', function (err, stdout, stderr) {
//                   // console.log(stderr);
//                    console.log(stdout);
////                    console.log('stdout');
////                    console.log(stdout.data);
////                    console.log(stdout.data[0]);
//                    res.send(stdout);
//                    //console.log(err);
//            });
//            //res.send('Action complete successfully');
//        } else
//        {
//            res.status(202).json('you have not permission to access settings');
//        }
//    });
//};
exports.deleteCrawlerData = function (req, res) {
    var id = req.params.userId;
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function (err, result) {

        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            TwitterModel.remove({}, function (err1, setting) {
                if (err1) {
                    res.status(202).json('Data Delettion Failure.');
                }
                res.send('Data Successfully Deteted.');
            });
        } else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });
};

