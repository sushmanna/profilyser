var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var user = require('../models/user');
var mongoose = require('mongoose');
var UserModel = mongoose.model('User');

exports.forgotpassword = function(req, res) {
    var transporter = nodemailer.createTransport(smtpTransport({
        host: 'mail.profilyser.com',
        port: 25,
        auth: {
            user: 'no-reply@profilyser.com',
            pass: 'y%{D`j^@2Rhx3>{j'
        },
        tls: {rejectUnauthorized: false},
        debug: true
    })
            );
    console.log(req.body.email);
    var email = req.body.email;
    var query = UserModel.findOne({});
    query.where('email').equals(email);
    query.exec(function(err, userResult) {

        if (userResult == null)
        {
            var response = {};
            response.message = 'Please Enter Valid Email.';
            res.status(202).json(response);
        } else
        {
            var randompass = Math.random().toString(36).substring(7);
            var passdata = {password: UserModel.schema.methods.encrypt(randompass)};
            UserModel.findByIdAndUpdate(userResult._id, {$set: passdata}, {'new': true}, function(err, data) {
                if (data == null)
                {
                    var response = {};
                    response.message = 'update failed';
                    res.status(202).json(response);
                } else
                {
                    var to = req.body.email;
                    var toDisplayName = typeof req.body.to_display_name !== "undefined" ? req.body.to_display_name : req.body.to;
                    var replyTo = typeof req.body.reply_to !== "undefined" ? req.body.reply_to : false;
                    var replyToDisplayName = typeof req.body.reply_to_display_name !== "undefined" ? req.body.reply_to_display_name : req.body.reply_to;
                    var message = '<div><table style="background-color: #f9f3e0; width: 100%;" border="0" cellspacing="16" cellpadding="0"><tbody><tr><td style="font-size: 12px;" align="left" valign="top" width="580"><p>As requested,here is a&nbsp; password to log into your&nbsp;.</p>';
                    message += '<p><br /> <strong>Password:' + randompass + '</strong></p>';
                    message += '</td></tr></tbody></table></div>';
                    var subject = 'Request For Reset Password.';


                    var mailsettings = {
                        from: 'Profilyser<no-reply@profilyser.com>',
                        to: to,
                        subject: subject,
                        html: message,
                        headers: {'reply-to': ''}
                    };

                    if (replyTo) {
                        mailsettings.headers = {'reply-to': replyToDisplayName + '<' + replyTo + '>'};
                    }

                    var response = {status: 0, message: ''};

                    transporter.sendMail(mailsettings, function(error, info) {
                        if (error) {
                            response.message = 'Your request could not process now.';
                            res.json(error);
                        }
                        response.status = 1;
                        response.message = 'Message successfully send.please check your Email.';
                        res.json(response);
                    });

                }
            });
        }
    });
};

exports.updateUser = function(req, res) {

    //console.log(req.body);
    if (req.body.password)
    {
        req.body.password = UserModel.schema.methods.encrypt(req.body.password);
    }
    else
    {
        delete req.body.password;
    }
    var id = req.body.id;
    var query = UserModel.findOne({});
    query.where('_id').ne(id);
    query.where('email').equals(req.body.email);
    query.exec(function(err, result) {
        if (result == null)
        {
            UserModel.findByIdAndUpdate(req.body.id, {$set: req.body}, {'new': true}, function(err, data) {
                if (data == null)
                {
                    res.status(202).json('update failed');
                } else
                {

                    var userdetails = {
                        id: data._id,
                        name: data.name,
                        email: data.email,
                        phone: data.phone,
                        address: data.address,
                        userDetails: data.userDetails,
                        userimage: data.userimage
                    };
                    res.json(userdetails);
                }
            });
        } else
        {
            res.status(202).json('Email id already assign with another user');
        }
    });
};
exports.getUser = function(req, res) {
    var id = req.params.id;
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function(err, result) {
        if (result == null)
        {
            res.status(202).json('Invalid Id.');
        } else
        {
            var userdetails = {
                id: result._id,
                name: result.name,
                email: result.email,
                address: result.address,
                phone: result.phone,
                twitterId: result.twitterId,
                userDetails: result.userDetails,
                userimage: result.userimage
            };
            res.json(userdetails);
        }
    });
};
exports.sendmail = function(req, res) {
    var transporter = nodemailer.createTransport(smtpTransport({
        host: 'mail.profilyser.com',
        port: 25,
        auth: {
            user: 'no-reply@profilyser.com',
            pass: 'y%{D`j^@2Rhx3>{j'
        },
        tls: {rejectUnauthorized: false},
        debug: true
    })
            );

    var to = req.body.to;
    var toDisplayName = typeof req.body.to_display_name !== "undefined" ? req.body.to_display_name : req.body.to;
    var replyTo = typeof req.body.reply_to !== "undefined" ? req.body.reply_to : false;
    var replyToDisplayName = typeof req.body.reply_to_display_name !== "undefined" ? req.body.reply_to_display_name : req.body.reply_to;
    var message = req.body.message;
    var subject = req.body.subject;

    var mailsettings = {
        from: 'Profilyser<no-reply@profilyser.com>',
        to: to,
        subject: subject,
        text: message,
        headers: {'reply-to': ''}
    };

    if (replyTo) {
        mailsettings.headers = {'reply-to': replyToDisplayName + '<' + replyTo + '>'};
    }

    var response = {status: 0, message: ''};

    transporter.sendMail(mailsettings, function(error, info) {
        console.log(info);
        console.log('error');console.log(error);
        if (error) {
            response.message = 'Your request could not process now.';
            res.json(error);
        }
        response.status = 1;
        response.message = 'Thanks for contacting us, we will respond you soon.';
        res.json(response);
    });

};
