var setting = require('../models/setting');
var twitter = require('../models/tweet');
var Twitter = require('twitter');
var $q = require('q');
var mongoose = require('mongoose');
var SettingModel = mongoose.model('Setting');
var TwitterModel = mongoose.model('Twitter');
var twitterDetails = {};
var client = {};
SettingModel.where('key').in(['consumer_key', 'consumer_secret', 'access_token_key', 'access_token_secret']).exec(callback);

function callback(err, result) {
    for (x in result) {
        twitterDetails[result[x].key] = result[x].value;
    }
    var client = new Twitter(twitterDetails);
}
exports.sampleapi = function(req, res) {
    var client = new Twitter(twitterDetails);
    //var params = {q: 'modi',geocode:'48.8567,2.3508,1mi'};
    var params = {
        geocode: '48.8567,2.3508,1mi'
    };
    client.get('search/tweets.json', params, function(error, tweets, response) {
        if (!error) {
            //console.log(tweets);
        }
        res.send(response);
    });
};
exports.sampleapibyclient = function(req, res) {
    var client = new Twitter(twitterDetails);
    var params = {
        locations: '-122.75,36.8,-121.75,37.8'
    };
    client.stream('statuses/filter', {
        track: 'twitter'
    }, function(stream) {
        stream.on('data', function(tweet) {
            console.log(tweet);
        });
        //        stream.on('error', function (error) {
        //            console.log(error);
        //        });
    });
};
exports.sampleapibyclient1 = function(req, res) {
    
var conn2     = mongoose.createConnection('mongodb://localhost/crawler');
var TwitterModel1 = conn2.model('Twitter');
TwitterModel1.find({}).sort('-time_of_publication').limit(50).exec(function(err1, Twitter) {
        if (err1) {
            res.status(202).json('No city Found');
        }
        res.send(Twitter);
    });
};
exports.getfamousprofiles = function(req, res) {
    var _match = {};
    if( req.query.to_city){
        _match.city = {$eq: req.query.to_city.toLowerCase()};
    }

    if( req.query.from_date && req.query.to_date){
         _match.time_of_publication = {"$gt": new Date(req.query.from_date), "$lte": new Date(req.query.to_date)};
    }

    /*if( req.query.search_tags){
        var keyword = req.query.search_tags;
        _match.tweet_content = {$regex: '[[:<:]]'+keyword+'[[:>:]]', $options: 'g'};
    }*/

    if( req.query.search_tags){
        var keyword = req.query.search_tags;
        var keywordarr = keyword.split(','); 

        if( keywordarr.length > 1){
            _match.$and = [ {$or: []}];
            for( i in keywordarr){
                keywordarr[i] = keywordarr[i].replace('@','');
                keywordarr[i] = keywordarr[i].replace('#','');
                _match.$and[0].$or.push({ tweet_content: {$regex: '[[:<:]]'+keywordarr[i]+'[[:>:]]', $options: 'i'}});
            }
        } else {
            keywordarr[0] = keywordarr[0].replace('@','');
            keywordarr[0] = keywordarr[0].replace('#','');
            _match.tweet_content = {$regex: '[[:<:]]'+keywordarr[0]+'[[:>:]]', $options: 'i'};
        }
    }

    /*if( req.query.search_tags){
        var tags = req.query.search_tags.split(',');
        var agg = [
            {$match:_match},
            {$project: {follower_number: 1, friend_number:1, TwitterId: 1, statuses_count: 1, hashtags: 1, entities: 1, geo: 1, coordinates: 1, twitter_place: 1, twitter_user: 1, keyWordExist: { $setIntersection: [ "$hashtags", tags ] }}},
            {$match:{keyWordExist: { $exists : true, $not: {$size: 0} }}}
        ];
    } else {*/
        var agg = [
            {$match:_match}
        ];
   /* }*/
    
    agg.push(
        {$group: 
            {
                _id: {
                    twitter_user_id: "$user_id"
                },
                screen_name: {
                    $last: "$screen_name",
                },
                full_name: {
                    $last: "$full_name",
                },
                profile_image_url:{
                    $last: "$profile_image_url",
                },
                profile_url:{
                    $last: "$profile_url",
                },
                city:{
                    $last: '$city',
                },
                user_id:{
                    $last: '$user_id',
                },
                crawled_tweet_count: {
                    $sum: 1
                },
                tweet_count: {
                    $last: "$statuses_count"
                },
                follower_number: {
                    $last: '$follower_number'
                },
                followees_number: {
                    $last: '$friend_number'
                }
            }
        }, 
        {$project: 
            {
                tweet_count: 1,
                follower_number: 1,
                followees_number: 1,
                point: {$subtract: ['$follower_number','$followees_number']},
                screen_name: 1,
                full_name: 1,
                profile_image_url: 1,
                profile_url: 1,
                city: 1,
                user_id: 1,
            }
        }, 
        {$sort: 
            {
                point: -1
            }
        },
        {$limit: 150}
    );

    TwitterModel.aggregate(agg, function(err, result) {
        if (err) {
            console.log(err);
        }
        res.send(result);
    });
};

exports.getbalancedprofiles = function(req, res) {

    var _match = {};
    if( req.query.to_city){
        _match.city = {$eq: req.query.to_city.toLowerCase()};
    }

    if( req.query.from_date && req.query.to_date){
         _match.time_of_publication = {"$gt": new Date(req.query.from_date), "$lte": new Date(req.query.to_date)};
    }

    if( req.query.search_tags){
        var keyword = req.query.search_tags;
        var keywordarr = keyword.split(',');
        //var keywordarr = keyword;
        if( keywordarr.length > 1){
            _match.$and = [ {$or: []}];
            for( i in keywordarr){
                keywordarr[i] = keywordarr[i].replace('@','');
                keywordarr[i] = keywordarr[i].replace('#','');
                _match.$and[0].$or.push({ tweet_content: {$regex: '[[:<:]]'+keywordarr[i]+'[[:>:]]', $options: 'i'}});
            }
        } else {
            keywordarr[0] = keywordarr[0].replace('@','');
            keywordarr[0] = keywordarr[0].replace('#','');
            _match.tweet_content = {$regex: '[[:<:]]'+keywordarr[0]+'[[:>:]]', $options: 'i'};
        }
    }

    /*if( req.query.search_tags){
        var tags = req.query.search_tags.split(',');
        var agg = [
            {$match:_match},
            {$project: {follower_number: 1, friend_number: 1, TwitterId: 1, hashtags: 1, entities: 1, geo: 1, coordinates: 1, twitter_place: 1, twitter_user: 1, keyWordExist: { $setIntersection: [ "$hashtags", tags ] }}},
            {$match:{keyWordExist: { $exists : true, $not: {$size: 0} }}}
        ];
    } else {*/
        var agg = [
            {$match:_match}
        ];
    /*}*/
    
    agg.push(
        {$group: 
            {
                _id: {
                    twitter_user_id: "$user_id"
                },
                screen_name: {
                    $last: "$screen_name",
                },
                full_name: {
                    $last: "$full_name",
                },
                profile_image_url:{
                    $last: "$profile_image_url",
                },
                profile_url:{
                    $last: "$profile_url",
                },
                city:{
                    $last: '$city',
                },
                user_id:{
                    $last: '$user_id',
                },
                crawled_tweet_count: {
                    $sum: 1
                },
                tweet_count: {
                    $last: "$statuses_count"
                },
                follower_number: {
                    $last: '$follower_number'
                },
                followees_number: {
                    $last: '$friend_number'
                }
            },
        }, 
        {$project: 
            {
                tweet_count: 1,
                follower_number: 1,
                followees_number: 1,
                screen_name: 1,
                full_name: 1,
                profile_image_url: 1,
                profile_url: 1,
                city: 1,
                user_id: 1,
                point: {
                    $add:[ {$divide: [{$subtract: ['$follower_number','$followees_number']}, '$tweet_count']}, 1]
                }
            }
        },
        {$sort: 
            {
                point: -1
            }
        },
        {$limit: 150}
    );


    TwitterModel.aggregate(agg, function(err, result) {
        if (err) {
            console.log(err);
        }
        res.send(result);
    });
};

exports.getpromisingprofiles = function(req, res) {
    var _match = {};
    if( req.query.to_city){
        _match.city = {$eq: req.query.to_city.toLowerCase()};
    }

    if( req.query.from_date && req.query.to_date){
         _match.time_of_publication = {"$gt": new Date(req.query.from_date), "$lte": new Date(req.query.to_date)};
    }

    if( req.query.search_tags){
        var keyword = req.query.search_tags;
        var keywordarr = keyword.split(',');
        //var keywordarr = keyword;
        if( keywordarr.length > 1){
            _match.$and = [ {$or: []}];
            for( i in keywordarr){
                keywordarr[i] = keywordarr[i].replace('@','');
                keywordarr[i] = keywordarr[i].replace('#','');
                _match.$and[0].$or.push({ tweet_content: {$regex: '[[:<:]]'+keywordarr[i]+'[[:>:]]', $options: 'i'}});
            }
        } else {
            keywordarr[0] = keywordarr[0].replace('@','');
            keywordarr[0] = keywordarr[0].replace('#','');
            _match.tweet_content = {$regex: '[[:<:]]'+keywordarr[0]+'[[:>:]]', $options: 'i'};
        }
    }

    /*if( req.query.search_tags){
        var tags = req.query.search_tags.split(',');
        var agg = [
            {$match:_match},
            {$project: {follower_number: 1, friend_number: 1, TwitterId: 1, hashtags: 1, entities: 1, geo: 1, coordinates: 1, twitter_place: 1, twitter_user: 1, keyWordExist: { $setIntersection: [ "$hashtags", tags ] }}},
            {$match:{keyWordExist: { $exists : true, $not: {$size: 0} }}}
        ];
    } else {*/
        var agg = [
            {$match:_match}
        ];
    /*}*/
    
    agg.push(
        {$group: 
            {
                _id: {
                    twitter_user_id: "$user_id"
                },
                screen_name: {
                    $last: "$screen_name",
                },
                full_name: {
                    $last: "$full_name",
                },
                profile_image_url:{
                    $last: "$profile_image_url",
                },
                profile_url:{
                    $last: "$profile_url",
                },
                city:{
                    $last: '$city',
                },
                user_id:{
                    $last: '$user_id',
                },
                crawled_tweet_count: {
                    $sum: 1
                },
                tweet_count: {
                    $last: "$statuses_count"
                },
                follower_number: {
                    $last: '$follower_number'
                },
                followees_number: {
                    $last: '$friend_number'
                }
            },
        }, 
        {$project: 
            {
                tweet_count: 1,
                follower_number: 1,
                followees_number: 1,
                screen_name: 1,
                full_name: 1,
                profile_image_url: 1,
                profile_url: 1,
                city: 1,
                user_id: 1,
                point: {
                    $add:[ {$divide: [{$subtract: ['$follower_number','$followees_number']}, {$multiply:['$tweet_count','$tweet_count']}]}, 1]
                }
            }
        }, 
        {$sort: 
            {
                point: -1
            }
        },
        {$limit: 150}
    );

    TwitterModel.aggregate(agg, function(err, result) {
        if (err) {
            console.log(err);
        }
        res.send(result);
    });
};

exports.getmaptweets = function(req, res) {
    var _match = {};
    _match.latitude = {$ne: null};
    _match.longitude = {$ne: null};

    if( req.query.to_city){
        _match.city = {$eq: req.query.to_city.toLowerCase()};
    }

    if( req.query.from_date && req.query.to_date){
         _match.time_of_publication = {"$gt": new Date(req.query.from_date), "$lte": new Date(req.query.to_date)};
    }

    if( req.query.search_tags){
        var keyword = req.query.search_tags;
        var keywordarr = keyword.split(',');
        //var keywordarr = keyword;
        if( keywordarr.length > 1){
            _match.$and = [ {$or: []}];
            for( i in keywordarr){
                keywordarr[i] = keywordarr[i].replace('@','');
                keywordarr[i] = keywordarr[i].replace('#','');
                _match.$and[0].$or.push({ tweet_content: {$regex: '[[:<:]]'+keywordarr[i]+'[[:>:]]', $options: 'i'}});
            }
        } else {
            keywordarr[0] = keywordarr[0].replace('@','');
            keywordarr[0] = keywordarr[0].replace('#','');
            _match.tweet_content = {$regex: '[[:<:]]'+keywordarr[0]+'[[:>:]]', $options: 'i'};
        }
    }

    /*if( req.query.search_tags){
        var keyword = req.query.search_tags;
        _match.tweet_content = {$in: [keyword]};
    }*/

    if( req.query.user_id){
        _match.user_id = {$eq: req.query.user_id};
    }

    /*if( req.query.search_tags){
        var tags = req.query.search_tags.split(',');
        var agg = [
            {$match:_match},
            {$project: {follower_number: 1, TwitterId: 1, tweet_content: 1, latitude: 1, longitude: 1, statuses_count: 1, time_of_publication: 1, user_Id: 1, hashtags: 1, entities: 1, geo: 1, coordinates: 1, twitter_place: 1, twitter_user: 1, keyWordExist: { $setIntersection: [ "$hashtags", tags ] }}},
            {$match:{keyWordExist: { $exists : true, $not: {$size: 0} }}}
        ];
    } else {*/
        var agg = [
            {$match:_match}
        ];
   /* }*/

    agg.push(
        {$project: 
            {
                tweet_content: 1,
                latitude: 1, 
                longitude: 1, 
                time_of_publication: 1,
                follower_number: 1,
                statuses_count: 1, 
                screen_name: 1,
                full_name: 1,  
                user_id: 1, 
                hashtags: 1
            }
        }, 
        {$sort: 
            {
                time_of_publication: -1
            }
        }
    );

    agg.push({$limit: 1000});

    /*agg.push({options: {allowDiskUse: true}});*/

    TwitterModel.aggregate(agg, function(err, result) {
        if (err) {
            console.log(err);
        }

        var tweetcount = {};
        for( i in result){
            tweetcount[result[i].user_id] = (tweetcount[result[i].user_id] || 0) + 1;
        }
        _res = {data:result, matchtweetcount: tweetcount};
        res.send(_res);
    });
};

exports.gettweets = function(req, res) {
    var _match = {};
    

    if( req.query.to_city){
        _match.city = {$eq: req.query.to_city.toLowerCase()};
    }

    if( req.query.from_date && req.query.to_date){
         _match.time_of_publication = {"$gt": new Date(req.query.from_date), "$lte": new Date(req.query.to_date)};
    }

    if( req.query.search_tags){
        var keyword = req.query.search_tags;
        var keywordarr = keyword.split(',');
        //var keywordarr = keyword;
        if( keywordarr.length > 1){
            _match.$and = [ {$or: []}];
            for( i in keywordarr){
                keywordarr[i] = keywordarr[i].replace('@','');
                keywordarr[i] = keywordarr[i].replace('#','');
                _match.$and[0].$or.push({ tweet_content: {$regex: '[[:<:]]'+keywordarr[i]+'[[:>:]]', $options: 'i'}});
            }
        } else {
            keywordarr[0] = keywordarr[0].replace('@','');
            keywordarr[0] = keywordarr[0].replace('#','');
            _match.tweet_content = {$regex: '[[:<:]]'+keywordarr[0]+'[[:>:]]', $options: 'i'};
        }
    }

    if( req.query.user_id){
        _match.user_id = {$eq: req.query.user_id};
    }

    
    var agg = [
        {$match:_match}
    ];
   
    agg.push(
        {$project: 
            {
                tweet_content: 1,
                latitude: 1, 
                longitude: 1, 
                time_of_publication: 1,
                statuses_count: 1, 
                screen_name: 1,
                full_name: 1,  
                user_id: 1, 
                friend_number: 1,
                follower_number: 1,
                statuses_count: 1,
                profile_image_url: 1,
                profile_url: 1,
                twitter_id: 1,
                hashtags: 1
            }
        }, 
        {$sort: 
            {
                time_of_publication: -1
            }
        }
    );

    TwitterModel.aggregate(agg, function(err, result) {
        if (err) {
            console.log(err);
        }
        res.send(result);
    });

};


exports.searchtweets = function( req, res){
    var query = TwitterModel.find({});

};