var setting = require('../models/cmscontent');
var $q = require('q');

var mongoose = require('mongoose');
var CmsModel = mongoose.model('Cms');
var UserModel = mongoose.model('User');

exports.listCms = function(req, res) {
    var id = req.params.id;
    var query = UserModel.findOne({});
    query.where('_id').equals(id);
    query.exec(function(err, result) {

        var access = result.roles.indexOf("admin");
        if (access != -1)
        {
            CmsModel.find({}, function(err1, setting) {
                res.send(setting);
            });
        } else
        {
            res.status(202).json('you have not permission to access settings');
        }
    });
};
exports.getCms = function(req, res) {

    var userId = req.params.userId;
    var query = UserModel.findOne({});
    query.where('_id').equals(userId);
    query.exec(function(err, result) {
        if (result == null)
        {
            res.status(202).json('Invalid Id.');
        }
        else
        {
            var access = result.roles.indexOf("admin");
            if (access != -1)
            {

                var cmsId = req.params.cmsId;
                var query = CmsModel.findOne({});
                query.where('_id').equals(cmsId);
                query.exec(function(err, result1) {
                    if (result1 == null)
                    {
                        res.status(202).json('Invalid Id.');
                    } else
                    {
                        var cmsdetails = {
                            id: result1._id,
                            name: result1.name,
                            content: result1.content,
                        };
                        res.json(result1);
                    }
                });
            } else
            {
                res.status(202).json('you have not permission to access settings');
            }
        }
    });

    //console.log(req.body);



};
exports.getContent = function(req, res) {
    var cmsKey = req.params.cmsKey;
    var query = CmsModel.findOne({});
    query.where('key').equals(cmsKey);
    query.exec(function(err, result1) {
        if (result1 == null)
        {
            res.status(202).json('Invalid url');
        } else
        {
            var cmsdetails = {
                name: result1.name,
                content: result1.content
            };
            res.json(result1);
        }
    });
};
exports.updateCms = function(req, res) {

    var userId = req.params.userId;
    var query = UserModel.findOne({});
    query.where('_id').equals(userId);
    query.exec(function(err, result) {


        if (result == null)
        {
            res.status(202).json('Invalid Id.');
        }
        else
        {
            var access = result.roles.indexOf("admin");
            if (access != -1)
            {

                var cmsId = req.params.cmsId;
                console.log('cmsId');
                console.log(cmsId);
                console.log('cmsId');
                var getData = {
                    content: req.body.content,
                    name: req.body.name
                };

                var conditions = {_id: cmsId}
                , update = {$set: getData}
                , options = {multi: true};

                CmsModel.update(conditions, update, options, callback);

                function callback(err, numAffected) {
                    if (err)
                    {
                        res.json(err);
                    } else
                    {
                        res.json(numAffected);
                    }
                    // numAffected is the number of updated documents
                }
                ;
//                CmsModel.findByIdAndUpdate(cmsId, {$set: req.body}, {'new': true}, function (err, data) {
//                    if (data == null || err)
//                    {
//                        res.status(202).json(err);
//                    } else
//                    {
//                        res.json(data);
//                    }
//                });
            } else
            {
                res.status(202).json('you have not permission to update settings');
            }
        }
    });

    //console.log(req.body);



};

