'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var _ = require('lodash');
var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var password = 'DTS-sustyhjty-1209hjytj';

/**
 * Getter
 */
var escapeProperty = function (value) {
    return _.escape(value);
};
var validateUniqueEmail = function (value, callback) {
    if (value != null) {
    var User = mongoose.model('User');
    User.find({
        $and: [{
                email: value
            }, {
                _id: {
                    $ne: this._id
                }
            }]
    }, function (err, user) {
        callback(err || user.length === 0);
    });
    }else
    {
        callback(true);
    }
    
};
var validateUniqueTwitterId = function (value, callback) {
    if (value != null) {
        var User = mongoose.model('User');
        User.find({
            $and: [{
                    twitterId: value
                }, {
                    _id: {
                        $ne: this._id
                    }
                }]
        }, function (err, user) {
            callback(err || user.length === 0);
        });
    } else
    {
        callback(true);
    }
};
var validateUniqueUsername = function (value, callback) {
    var User = mongoose.model('User');
    User.find({
        $and: [{
                username: value
            }, {
                _id: {
                    $ne: this._id
                }
            }]
    }, function (err, user) {
        callback(err || user.length === 0);
    });
};
/**
 * User Schema
 */
var UserSchema = new Schema({
    name: {
        type: String,
    },
    twitterId: {
        type: String,
        get: escapeProperty,
        validate: [validateUniqueTwitterId, 'Twitter user already inserted']
    },
    username: {
        type: String,
        unique: true,
        required: true,
        get: escapeProperty,
        validate: [validateUniqueUsername, 'Username is already in-use']
    },
    email: {
        type: String,
        match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'Please enter a valid email'],
        validate: [validateUniqueEmail, 'E-mail address is already in-use']
    },
    roles: {
        type: Array,
        default: ['user']
    },
    userDetails: {
        type: String,
    },
    userimage: {
        type: String,
    },
    password: {
        type: String,
    },
    address: {
        type: String,
    },
    phone: {
        type: String,
    },
    type: {
        type: String,
    },
});
/**
 * Pre-save hook
 */
UserSchema.pre('save', function (next) {
    var s = this.encrypt(this.password);
    this.password = s;
    next();
});
/**
 * Pre-update hook
 */
//UserSchema.pre('update', function(next) {
//    console.log('ssss');
//    var s = this.encrypt(this.password);
//    this.password = s;
//    next();
//});

/**
 * Methods
 */
UserSchema.methods = {
    isAdmin: function () {
        return this.roles.indexOf('admin') !== -1;
    },
    authenticate: function (plainText) {
        return this.encrypt(plainText) === this.password;
    },
    encrypt: function (text) {
        var cipher = crypto.createCipher(algorithm, password);
        var crypted = cipher.update(text, 'utf8', 'hex')
        crypted += cipher.final('hex');
        return crypted;
    },
    decrypt: function (text) {
        var decipher = crypto.createDecipher(algorithm, password);
        var dec = decipher.update(text, 'hex', 'utf8')
        dec += decipher.final('utf8');
        return dec;
    }
};
mongoose.model('User', UserSchema);
