'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CmsSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    key: {
        type: String,
        required: true,
        unique: true,
    },
    content: {
        type: String,
        required: true,
    }
});
mongoose.model('Cms', CmsSchema);
