'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SettingSchema = new Schema({
    key: {
        type: String,
        required: true,
        unique: true,
    },
    label: {
        type: String,
        required: true,
        unique: true,
    },
    value: {
        type: String,
        required: true,
    }
});
mongoose.model('Setting', SettingSchema);
