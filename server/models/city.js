'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CitySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    west_longitude: {
        type: String,
        required: true,
    },
    south_latitude: {
        type: String,
        required: true,
    },
    east_longitude: {
        type: String,
        required: true,
    },
    north_latitude: {
        type: String,
        required: true,
    },
    consumer_key: {
        type: String,
        required: true
    },
    consumer_secret: {
        type: String,
        required: true
    },
    access_token_key: {
        type: String,
        required: true
    },
    access_token_secret: {
        type: String,
        required: true
    },
    isCrawler: {
        type: Boolean,
        default: false
    },
    allowFreeAccess: {
        type: Boolean,
        default: false
    },
    isDefault: {
        type: Boolean,
        default: false
    }
});
mongoose.model('City', CitySchema);
