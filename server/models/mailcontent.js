'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var MailSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    key: {
        type: String,
        required: true,
        unique: true,
    },
    subject: {
        type: String,
        required: true,
    },
    template: {
        type: String,
        required: true,
    }
});
mongoose.model('Mail', MailSchema);
