'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var twitterSchema = new Schema({
    twitter_id: {
        type: String,
        required: true,
        unique: true
    },
    tweet_content: {
        type: String,
        required: true
    },
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    city: {
        type: String,
        required: true
    },
    time_of_publication: {
        type: Date,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    screen_name: {
        type: String,
        required: true
    },
    profile_url: {
        type: String,
        required: true
    },
    friend_number: {
        type: Number,
        required: true
    },
    follower_number: {
        type: Number,
        required: true
    },
    profile_created_dt: {
        type: Date,
        required: true
    },
    profile_image_url: {
        type: String
    },
    statuses_count: {
        type: Number,
        required: true
    },
    rew_tweet_count: {
        type: Number,
        required: true
    },
    lang: {
        type: String
    },
    source: {
        type: String
    },
    full_name: {
        type: String
    },
    location: {
        type: String
    },
    favourites: {
        type: Number
    }
});
mongoose.model('Twitter', twitterSchema);
