Project requirement in server mode
1. mongodb
2. node server
3. npm 


Project requirement in development mode
1. mongodb
2. node server
3. npm
4. Run “sh project-global-setuplinux.sh” for Linux system and “sh project-global-setupwin.sh” for Windows system system 


TO SETUP THE PROJECT RUN IN TERMINAL
1.  Run “sh project-setup.sh”


TO RUN THE PROJECT RUN IN TERMINAL
1. make sure your database server is running mode. Otherwise we have created a file to setup database named “database-start.sh”. to setup using this  Just stop the mongod service and create a folder named “database” and run “sh database-start.sh” so database will created in database folder.
2. Run “node server”.


TO DEVELOP THE PROJECT ALWAYS RUN IN TERMINAL
1. When you changing your code always you have to run "node watch-webpack-browser.js" in terminal.


CODE STRUCTURE
All the file in frontend are in "src" folder and all the server file are in "server" folder. The project are setup as pure angular.js and node js structure.


REMIND WHEN UPLOADING IN SERVER
1. Uncomment the google analytics code in index.html file.
2. Change/Uncomment API Link for in config/global-constant.js  in module folder.
3. make sure the the project running in server using port 4689 in server.js
4. after all process you have to run "node watch-webpack-browser.js" and upload  "public" folder in server
5. restart the server using "forever restartall"