var webpack = require('webpack');
var ReloadPlugin = require('webpack-reload-plugin');
var path = require('path');
var fs = require('fs');
var cwd = process.cwd();
var version = require(path.resolve(cwd, 'package.json')).version;
var config = {
    context: path.resolve(cwd, "src"),
    entry: findEntries(['stylefrontAsset.js', 'styleadminAsset.js', 'scriptAsset.js']),
    output: {
        path: path.resolve(cwd, "public"),
        filename: "[name]/bundle.js",
        publicPath: '../'
    },
    module: {
        loaders: [
            {test: /\.json$/, loader: "json-loader"},
            {test: /\.coffee$/, loader: "coffee-loader"},
            {test: /\.css$/, exclude: /bootstrap\.css$/, loader: "style-loader!css-loader"},
            {test: /\.less$/, loader: "style-loader!css-loader!less-loader"},
            {test: /\.jade$/, loader: "jade-loader"},
            {test: /\.(png|jpg|gif)$/, loader: "file-loader?limit=50000&name=[path][name].[ext]"},
            {test: /\.eot?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.ttf?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.svg?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.html$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.woff2$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.mp4$/, loader: "file-loader?name=[path][name].[ext]"},
            {test: /\.webm$/, loader: "file-loader?name=[path][name].[ext]"}
        ]
    },
    /*plugins:[
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(version),
    }),
    new ReloadPlugin(false)
  ]*/
};
/*config.plugins.push(new webpack.optimize.UglifyJsPlugin({mangle:false}));*/
function addEntries(fileName, entries) {

    // mains = [ 'xxxxxx/src/rpm/main.js', .... ]
    var mains = require('glob').sync(path.resolve(cwd, 'src', '**', fileName));
    //console.log(mains);

    mains.forEach(function(file) {
        //console.log(file);

        // entry = ./rpm/main
        var entry = "./" + file.substr(5 + cwd.length, file.length - 8 - cwd.length);
        //console.log(entry);
        // name = rpm    
        var name = entry.substr(2, entry.length - 7) || './';
        // { 'rpm': './rpm/main' }
        entries[name] = entry;
        //console.log(name);
    });
}
function findEntries(fileNames) {
    var entries = {};
    fileNames.forEach(function(fileName) {
        addEntries(fileName, entries);
    });
    // console.log(entries);
    return entries;
}
/**
 * Enable minification
 */



module.exports = config;