var ServerSession;
module.exports = function(app) {
    var mongoose = require('mongoose');
    var user = require('../server/models/user');
    var UserModel = mongoose.model('User');
    require('./global-constant.js');

    var passport = require('passport');
    var LocalStrategy = require('passport-local').Strategy;
    var TwitterStrategy = require('passport-twitter').Strategy;

    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });
    passport.use(new TwitterStrategy({
        consumerKey: 'OJzLxQfs8d2p9yYxJwoW9bD4B',
        consumerSecret: 'uwHbmemaOKgBZlxBQVnrBSEnqufcqT6B6Z9WivA3Jbdfim54Qv',
        callbackURL: HOSTURL + "/twitterlogin/callback"
    }, function(token, tokenSecret, profile, done) {
        process.nextTick(function() {
            return done(null, profile);
        });
    }));

    //setup finction and api for login
    app.get('/api/twitterlogin', passport.authenticate('twitter'), function(req, res) {
    });
    app.get('/api/twitterlogin/callback', passport.authenticate('twitter', {failureRedirect: '/login'}), function(req, res) {
        ServerSession = req.session;
        if (ServerSession.userData && ServerSession.userData.id) {
            var query = UserModel.findOne({});
            query.where('twitterId').equals(req.user.id);
            query.exec(function(err, result) {
                if (result == null)
                {
                    var updateVal = {twitterId: req.user.id, userimage: req.user._json.profile_image_url, userDetails: req.user._json.description};
                    UserModel.findByIdAndUpdate(ServerSession.userData.id, {$set: updateVal}, {'new': true}, function(err, data) {
                        if (data == null)
                        {
                            res.redirect('/user/linktwitter');
                        } else
                        {
                            res.redirect('/user/linktwitter');
                        }
                    });
                } else
                {
                    res.redirect('/user/linktwitter');
                    //res.status(202).json('Update failed due to duplicate twitter account.Please login with twitter.');
                }
            });
        } else
        {
            var query = UserModel.findOne({});
            query.where('twitterId').equals(req.user.id);
            query.exec(function(err, result) {
                if (result == null)
                {
                    var userData = new UserModel(
                            {
                                name: req.user.displayName,
                                email: null,
                                username: req.user.username,
                                twitterId: req.user.id,
                                password: req.user.username + req.user.id,
                                userimage: req.user._json.profile_image_url,
                                userDetails: req.user._json.description,
                            });
                    userData.save(function(err, userData) {
                        if (err)
                        {
                            console.log('err:' + err);
                        } else
                        {
                            var userdetails = {
                                id: userData._id,
                                role: userData.roles,
                                name: userData.name,
                                twitterId: userData.twitterId,
                                userimage: userData.userimage,
                                userDetails: userData.userDetails,
                            };
                            ServerSession = req.session;
                            ServerSession.userData = userdetails;
                        }
                        res.redirect('/login');
                    });
                } else
                {
                    var userdetails = {
                        id: result._id,
                        role: result.roles,
                        name: result.name,
                        twitterId: result.twitterId,
                        userimage: result.userimage,
                        userDetails: result.userDetails
                    };
                    ServerSession = req.session;
                    ServerSession.userData = userdetails;
                    res.redirect('/login');
                }
            });
        }



    });
    app.post('/api/register', function(req, res) {
        var userData = new UserModel(
                {
                    name: req.body.name,
                    email: req.body.email,
                    username: req.body.username,
                    twitterId: null,
                    password: req.body.password
                });
        userData.save(function(err, userData) {
            if (err)
            {
                res.status(202).json(err);
            }
            res.json(userData);
        });
    });
    app.post('/api/login', function(req, res) {
        var username = req.body.username;
        var password = UserModel.schema.methods.encrypt(req.body.password);
        var query = UserModel.findOne({});
        query.where('username').equals(username);
        query.where('password').equals(password);
        query.exec(function(err, result) {
            if (result == null)
            {
                res.status(202).json('Invalid credential.');
            } else
            {
                var userdetails = {
                    id: result._id,
                    role: result.roles,
                    name: result.name
                };
                ServerSession = req.session;
                ServerSession.userData = userdetails;
                res.json(userdetails);
            }
        });
    });
    app.get('/api/logout', function(req, res) {
        req.session.destroy(function(err) {
            if (err) {
                console.log(err);
            }
            else
            {
                res.redirect('/');
            }
        });
    });
    app.get('/api/isuserlogin', function(req, res) {
        ServerSession = req.session;
        if (ServerSession.userData)
        {
            res.json(ServerSession.userData);
        }
        else
        {
            res.json(null);
        }
    });
};