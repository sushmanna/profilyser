var _ = require('lodash');
angular.module('user', []);
angular.module('AdminService', []);
angular.module('UserService', []);
angular.module('MessageService', []);
angular.module('TwitterService', []);
angular.module('mapDirective', []);
angular.module('cloudDirective', []);
angular.module('product', ['ngMap']);
angular.module('home', []);
angular.module('admin', []);
angular.module('demo', []);
angular.module('contact', []);




angular.module('app', ['ngRoute', 'ngScrollbars', 'ngMap', 'cloudDirective', 'mapDirective', 'ngSanitize', 'angularMoment', 'ngCsv', 'ngStorage', 'ngResource', 'ngMessages', 'ngBootbox', 'ui.bootstrap', 'ngTagsInput', 'AdminService', 'UserService', 'TwitterService', 'MessageService', 'user', 'home', 'product', 'admin', 'demo', 'contact','wysiwyg.module'])
        .config(function($routeProvider, $locationProvider) {
            $routeProvider
                    .otherwise({
                        redirectTo: '/'
                    });
            $locationProvider.html5Mode(true);
        })
        .run(function($sessionStorage, $location, $window, UserService, $rootScope) {
            $rootScope.$on('$routeChangeStart', function() {
                var allowAccess = ['login', 'register'];
                var urlArr = $location.$$path.split("/");
                var access = allowAccess.indexOf(urlArr[1]);
                if (access != -1) {
                    UserService.isUserLogin().success(function(response, status) {
                        if (response != null)
                        {
                            $sessionStorage.loginData = response;
                            UserService.loginRedirect(response);
                        }
                    });
                    if (angular.isDefined($sessionStorage.loginData)) {
                        UserService.loginRedirect($sessionStorage.loginData);
                    }
                }
            });
            /*for admin access start*/
            var urlArr = $location.$$path.split("/");
            if (urlArr[1] == 'admin') {
                if (angular.isDefined($sessionStorage.loginData)) {
                    var access = $sessionStorage.loginData.role.indexOf("admin");
                    if (access == -1) {
                        //if not have access
                        $window.location.href = '/';
                    }
                } else {
                    //if not sign in or previous signed
                    UserService.isUserLogin().success(function(response, status) {
                        if (response != null)
                        {
                            $sessionStorage.loginData = response;
                        } else
                        {
                            $window.location.href = '/';
                        }
                    });

                }
            }
            if (urlArr[1] == 'user') {
                if (angular.isDefined($sessionStorage.loginData)) {
                    var access = $sessionStorage.loginData.role.indexOf("user");
                    if (access == -1) {
                        //if not have access
                        $window.location.href = '/';
                    }
                } else {
                    //if not sign in or previous signed
                    UserService.isUserLogin().success(function(response, status) {
                        if (response != null)
                        {
                            $sessionStorage.loginData = response;
                        } else
                        {
                            $window.location.href = '/';
                        }
                    });

                }
            }
            /*for admin access end*/
        })
        .factory('serverUrls', serverUrls);
serverUrls.$inject = [];

function serverUrls() {

    var urls = {
        login: HOSTURL + '/login',
        logout: HOSTURL + '/logout',
        forgot: HOSTURL + '/forgotpassword',
        isUserLogin: HOSTURL + '/isuserlogin',
        register: HOSTURL + '/register',
        profile: HOSTURL + '/profile',
        settings: HOSTURL + '/settings',
        cmslist: HOSTURL + '/cmslist',
        getcms: HOSTURL + '/cms',
        getcmsContent: HOSTURL + '/cmscontent',
        citylist: HOSTURL + '/citylist',
        managecity: HOSTURL + '/managecity',
        getcity: HOSTURL + '/getcity',
        defaultcity: HOSTURL + '/defaultcity',
        sendmail: HOSTURL + '/sendmail',
        crawlerDay: HOSTURL + '/crawlerday',
        deleteCrawler: HOSTURL + '/deletecrawlerdata',
        crawlerAction: HOSTURL + '/crawleraction',
        crawlerCityStatus: HOSTURL + '/crawlercitystatus',
        crawlerCityAccess: HOSTURL + '/crawlercityaccess',
        //crawlerStatus: HOSTURL + '/crawlerstatus'
        getResult: HOSTURL + '/test',
        getResult1: HOSTURL + '/test1',
        getFamousProfiles: HOSTURL + '/getfamousprofiles',
        getBalancedProfiles: HOSTURL + '/getbalancedprofiles',
        getPromisingProfiles: HOSTURL + '/getpromisingprofiles',
        getTweets: HOSTURL + '/gettweets',
        getMapTweets: HOSTURL + '/getmaptweets',
        getCityByQuery: HOSTURL + '/getcitybyquery'
    };
    return urls;
}