(function() {
    'use strict';
    var twitter = require('twitter-text');
    angular
        .module('app')
        .filter('mathlog', mathlog)
        .filter('twodecimalplace', twodecimalplace)
        .filter('extracturl', extracturl)
        .filter('htmlfilter',htmlfilter );

    function mathlog() {
        return filter;

        function filter(input) {
            if (input !== null && typeof input !== "undefined") {
                return Math.log( input);
            }
        }
    }

    function twodecimalplace() {
        return filter;

        function filter(input) {
            if (input !== null && typeof input !== "undefined") {
                return input.toFixed(2);
            }
        }
    }

    function extracturl() {
        return filter;

        function filter(input) {
            if (input !== null && typeof input !== "undefined") {
                return twitter.autoLinkUrlsCustom(twitter.htmlEscape(input),{targetBlank:1});
            }
        }
    }
    
    htmlfilter.$inject=['$sce'];
    function htmlfilter($sce) { 
        return filter;
        function filter(input) {
            if (input !== null && typeof input !== "undefined") {
                return $sce.trustAsHtml(input);
            }
        }
    }
    
 })();