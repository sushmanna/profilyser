(function () {
    angular.module('contact').controller('ContactController', ContactController);
    ContactController.$inject = ['$scope', '$rootScope', '$location', 'UserService'];
    function ContactController($scope, $rootScope, $location, UserService) {
        var vm = this;
        vm.form = {
            reply_to_display_name: '',
            reply_to: '',
            //to: 'biswarup.das@profilyser.com',
            to: 'contact@profilyser.com',
            to_display_name: 'Profilyser Inquery',
            message: '',
            subject: 'Profilyser customer contact notify.'
        };

        vm.contactProfilsyer = function () {
            if (vm.form.name != '' && vm.form.from != '' && vm.form.message) {
                UserService.contactUs(vm.form).success(function (response, status)
                {
                    console.log(response);
                    if (response.status == 1)
                    {
                        vm.autoReply();
                    }
                });
            }
        };

        vm.autoReply = function () {
            var message = "Hi," + "\n" + "\n" +
                    "Thank you for contacting us. Your mail has been received and shall be processed shorly." + "\n" + "\n" +
                    "Best Regards," + "\n" +
                    "Profilyser Team";

            var autoreply = {
                to: vm.form.reply_to,
                reply_to_display_name: 'Profilyser Inquery',
                //reply_to: 'biswarup.das@profilyser.com',
                reply_to: 'contact@profilyser.com',
                to_display_name: 'Profilyser',
                message: message,
                subject: 'Thank you for your inquery.'
            };
            UserService.contactUs(autoreply).success(function (response, status)
            {
                console.log(response);
                if (response.status == 1)
                {
                    alert(response.message);
                    /*$ngBootbox.confirm('A question?')
                            .then(function () {
                                console.log('Confirmed!');
                            }, function () {
                                console.log('Confirm dismissed!');
                            });*/
                }
            });
        };
    }
})();