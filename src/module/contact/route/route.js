(function () {
    angular.module('contact').config(function ($routeProvider, $locationProvider) {
        $routeProvider
                .when('/contact', {
                    controller: 'ContactController',
                    templateUrl: '../module/contact/views/contact.html'
                })
    });
})();