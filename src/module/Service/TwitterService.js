(function() {
    angular.module('TwitterService').factory('TwitterService', TwitterService);

    TwitterService.$inject = ['$http', 'serverUrls', '$window', '$location'];

    function TwitterService($http, serverUrls, $window, $location) {
        var data = {};

        data.getFamousProfiles = function(params) {
            return $http({
                url: serverUrls.getFamousProfiles,
                method: 'GET',
                params: params,
                responseText: 'JSON'
            });
        };


        data.getBalancedProfiles = function(params) {
            return $http({
                url: serverUrls.getBalancedProfiles,
                method: 'GET',
                params: params,
                responseText: 'JSON'
            });
        };


        data.getPromisingProfiles = function(params) {
            return $http({
                url: serverUrls.getPromisingProfiles,
                method: 'GET',
                params: params,
                responseText: 'JSON'
            });
        };

        data.getTweets = function(formData, searchData) {
            //Object.assign(searchData, formData);
            for (var key in formData) {
                searchData[key] = formData[key];
            }
            return $http({
                url: serverUrls.getTweets,
                method: 'GET',
                params: searchData,
                responseText: 'JSON'
            });
        };

        data.getMapTweets = function(searchData) {
            return $http({
                url: serverUrls.getMapTweets,
                method: 'GET',
                params: searchData,
                responseText: 'JSON'
            });
        };

        return data;
    }
})();

