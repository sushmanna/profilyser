(function() {
    angular.module('AdminService').factory('AdminService', AdminService);

    AdminService.$inject = ['$http', 'serverUrls', '$window', '$location'];

    function AdminService($http, serverUrls, $window, $location) {
        var data = {};

        data.getSettings = function(userId) {
            return $http({
                url: serverUrls.settings + '/' + userId,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getCrawlerDay = function(userId) {
            return $http({
                url: serverUrls.crawlerDay + '/' + userId,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.updateSettings = function(settingsData) {
            return $http({
                url: serverUrls.settings,
                method: 'PUT',
                data: settingsData,
                responseText: 'JSON'
            });
        };
        data.updateCrawlerDay = function(settingsData) {
            return $http({
                url: serverUrls.crawlerDay + '/' + settingsData.id,
                method: 'PUT',
                data: settingsData.data,
                responseText: 'JSON'
            });
        };
        data.deleteCrawlerData = function(userId) {
            return $http({
                url: serverUrls.deleteCrawler + '/' + userId,
                method: 'DELETE',
                responseText: 'JSON'
            });
        };
        data.restartCrawler = function(userId) {
            var data = {action: 'pm2 restart all'}
            return $http({
                url: serverUrls.crawlerAction + '/' + userId,
                method: 'PUT',
                data: data,
                responseText: 'JSON'
            });
        };
        data.startCrawler = function(userId) {
            var data = {action: 'pm2 start crawler-production.js'}
            return $http({
                url: serverUrls.crawlerAction + '/' + userId,
                method: 'PUT',
                data: data,
                responseText: 'JSON'
            });
        };
        data.stopCrawler = function(userId) {
            var data = {action: 'pm2 delete all'}
            return $http({
                url: serverUrls.crawlerAction + '/' + userId,
                method: 'PUT',
                data: data,
                responseText: 'JSON'
            });
        };
//        data.restartCrawler = function (userId) {
//            var data={action:'forever restart crawler-production.js -monitor -production'}
//            return $http({
//                url: serverUrls.crawlerAction + '/' + userId,
//                method: 'PUT',
//                data: data,
//                responseText: 'JSON'
//            });
//        };
//        data.startCrawler = function (userId) {
//            var data={action:'forever start crawler-production.js -monitor -production'}
//            return $http({
//                url: serverUrls.crawlerAction + '/' + userId,
//                method: 'PUT',
//                data: data,
//                responseText: 'JSON'
//            });
//        };
//        data.stopCrawler = function (userId) {
//            var data={action:'forever stop crawler-production.js -monitor -production'}
//            return $http({
//                url: serverUrls.crawlerAction + '/' + userId,
//                method: 'PUT',
//                data: data,
//                responseText: 'JSON'
//            });
//        };
//        data.statusCrawler = function (userId) {
//            var data={action:'forever list'}
//            return $http({
//                url: serverUrls.crawlerStatus + '/' + userId,
//                method: 'PUT',
//                data: data,
//                responseText: 'JSON'
//            });
//        };

        //CMS manage
        data.getCmsList = function(userId) {
            return $http({
                url: serverUrls.cmslist + '/' + userId,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getCms = function(userId, cmsId) {
            return $http({
                url: serverUrls.getcms + '/' + userId + '/' + cmsId,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getCmsContent = function(cms) {
            return $http({
                url: serverUrls.getcmsContent + '/' + cms,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.updateCms = function(userId, cmsId, data) {
            return $http({
                url: serverUrls.getcms + '/' + userId + '/' + cmsId,
                method: 'PUT',
                data: data,
                responseText: 'JSON'
            });
        };
        //city manage
        data.getCityList = function() {
            return $http({
                url: serverUrls.citylist,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.createCity = function(data) {
            return $http({
                url: serverUrls.managecity,
                method: 'POST',
                data: data,
                responseText: 'JSON'
            });
        };
        data.getCity = function(cityId) {
            return $http({
                url: serverUrls.getcity + '/' + cityId,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.updateCity = function(cityId, data) {
            return $http({
                url: serverUrls.managecity + '/' + cityId,
                method: 'PUT',
                data: data,
                responseText: 'JSON'
            });
        };
        data.changeToDefaultCity = function(cityId) {
            return $http({
                url: serverUrls.defaultcity + '/' + cityId,
                method: 'PUT',
                responseText: 'JSON'
            });
        };
        data.changeCrawlerCityStatus = function(cityId, data) {
            var updateData = {status: data}
            return $http({
                url: serverUrls.crawlerCityStatus + '/' + cityId,
                method: 'PUT',
                data: updateData,
                responseText: 'JSON'
            });
        };
        data.changeCrawlerCityAccess = function(cityId, data) {
            var updateData = {status: data}
            return $http({
                url: serverUrls.crawlerCityAccess + '/' + cityId,
                method: 'PUT',
                data: updateData,
                responseText: 'JSON'
            });
        };
        data.deteleCity = function(cityId) {
            return $http({
                url: serverUrls.managecity + '/' + cityId,
                method: 'DELETE',
                responseText: 'JSON'
            });
        };


        data.getResult = function() {
            return $http({
                url: serverUrls.getResult,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getResult1 = function() {
            return $http({
                url: serverUrls.getResult1,
                method: 'GET',
                responseText: 'JSON'
            });
        };
        data.getCityByQuery = function(params) {
            return $http({
                url: serverUrls.getCityByQuery,
                method: 'GET',
                params: params,
                responseText: 'JSON'
            });
        };
        return data;
    }
})();
