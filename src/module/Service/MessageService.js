(function () {
    angular.module('MessageService').factory('MessageService', MessageService);

    MessageService.$inject = ['$http', 'serverUrls', '$window', '$location', '$ngBootbox'];

    function MessageService($http, serverUrls, $window, $location, $ngBootbox) {
        var data = {};
        $ngBootbox.setDefaults({
            animate: false,
            backdrop: false
        });
        data.alert = function (message, path) {
            var dialog = {
                show: true,
                closeButton: true,
                message: '<div class="alert alert-danger">' + message + '</div>'
            };
            if (path != null)
            {
                dialog.onEscape = function () {
                    $window.location.href = '/' + path;
                }
            }
            return $ngBootbox.customDialog(dialog);
        };
        data.success = function (message, path) {
            var dialog = {
                show: true,
                closeButton: true,
                message: '<div class="alert alert-success">' + message + '</div>'
            };
            if (path != null)
            {
                dialog.onEscape = function () {
                    $window.location.href = '/' + path;
                }
            }
            return $ngBootbox.customDialog(dialog);
        };
        return data;
    }
})();
