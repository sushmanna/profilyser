(function () {
    angular.module('demo').config(function ($routeProvider, $locationProvider) {
        $routeProvider
                .when('/demo', {
                    controller: 'DemoController',
                    templateUrl: '../module/demo/views/demo.html'
                })
    });
})();