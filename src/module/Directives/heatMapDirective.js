(function () {
//var Twitter = require('twitter');
    angular.module('mapDirective')
            .directive('showheatmap', showheatmap);
    function  showheatmap()
    {
        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.templateUrl = "module/Directives/templates/heatMap.html";
        directive.link = link;
        directive.scope = {mapdata: '=', center: '=', zoom: '='};
        function link(scope, element, attrs)
        {
            scope.mapdata = scope.mapdata;
            scope.center = scope.center;
            scope.zoom = scope.zoom;

            var heatmap;
            var gmap;
            function heatmapData() {
                var createpoint = [];
                for (var c in scope.mapdata)
                {
                    createpoint[c] = new google.maps.LatLng(scope.mapdata[c].latitude, scope.mapdata[c].longitude);
                }
                return createpoint;
            }
            var latlng = new google.maps.LatLng(scope.center[0], scope.center[1]);
            scope.$on('mapInitialized', function (event, map) {
                heatmap = new google.maps.visualization.HeatmapLayer({
                    data: heatmapData(),
                    center: latlng
                });
                google.maps.event.addListener(map, 'zoom_changed', function () {
                    scope.$emit('changeZoom', map.getZoom());
                });
                google.maps.event.addListener(map, 'center_changed', function () {
                    var a = [map.getCenter().lat(), map.getCenter().lng()];
                    scope.$emit('changeCenter', a);
                });
                gmap = map;
                google.maps.event.trigger(map, 'resize');
                map.setCenter(latlng);
                heatmap.setOptions({
                    dissipating: true,
                    maxIntensity: 10,
                    radius: 10,
                    opacity: 1
                });
                heatmap.setMap(map);
            });

        }
        return directive;
    }
})();
