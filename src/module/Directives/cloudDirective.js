(function() {
    angular.module('cloudDirective').directive('clouder', clouder);
    clouder.$inject = ['$document'];

    function clouder($document) {
        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.templateUrl = "module/Directives/templates/cloud.html";
        directive.link = link;
        directive.scope = {
            data: '='
        };

        function link(scope, element, attrs) {
            scope.elems = scope.data;
            var w = 960;
            var h = 268;
            var clouder = document.getElementById('clouder');
            clouder.style.width = w * 2 / 3 + 'px';
            clouder.style.height = h * 2 / 3 + 'px';
            clouder.style.position = "absolute";
            clouder.style.left = w / 6 + 'px';
            clouder.style.top = h / 6 + 'px';
            window.clouder = new Clouder({
                container: clouder,
                tags: scope.data,
                interval: 150
            });
        }
        return directive;
    }
})();