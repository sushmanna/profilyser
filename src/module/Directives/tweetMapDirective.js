(function() {
    var moment = require('moment');
    angular.module('mapDirective')
            .directive('showtweetmap', showtweetmap);
    showtweetmap.$inject = ['$document'];
    function  showtweetmap($document)
    {
        var directive = {};
        directive.restrict = 'EA'; /* restrict this directive to elements */
        directive.templateUrl = "module/Directives/templates/tweetMap.html";
        directive.link = link;
        directive.scope = {mapdata: '=', center: '=', zoom: '='};
        function link(scope, element, attrs)
        {
            var map;
            scope.positions = scope.mapdata;
            scope.center = scope.center;
            scope.zoom = scope.zoom;
            var infowindow = new google.maps.InfoWindow();
            var latlng = new google.maps.LatLng(scope.center[0], scope.center[1]);
            scope.$on('mapInitialized', function(event, map) {
                google.maps.event.addListener(map, 'zoom_changed', function() {
                    scope.$emit('changeZoom', map.getZoom());
                });
                google.maps.event.addListener(map, 'center_changed', function() {
                    var a= [map.getCenter().lat(),map.getCenter().lng()];
                    scope.$emit('changeCenter', a);
                });
                map.setCenter(latlng);
                google.maps.event.trigger(map, 'resize');
            });
            scope.showInfoWindow = function(event, p, positions) {
                if (infowindow) {
                    infowindow.close();
                }

                var txt = '';
                for( var i in positions){
                    if( p.latitude == positions[i].latitude && p.longitude == positions[i].longitude){
                        txt += '<div class="tweet-txt"><i class="fa fa-twitter"></i> ' + positions[i].tweet_content + '</div>';
                    }
                }

                var center = new google.maps.LatLng(p.latitude, p.longitude);
                infowindow.setContent('<div style="max-width:300px">' +txt+'<div class="map-info-bottom-wrap"><div><i class="fa fa-user"></i> <a href="http://twitter.com/' +p.screen_name+ '" target="_new">' + p.screen_name + '</a></div><div><i class="fa fa-clock-o"></i> ' + moment(p.time_of_publication).startOf('hour').fromNow() + '</div><div><i class="fa fa-users"></i> Followers: ' + p.follower_number + '</div><div><i class="fa fa-twitter"></i> Total: ' + p.statuses_count + '</div><div><i class="fa fa-twitter"></i> Match: ' + p.match + '</div></div></div>');
                infowindow.setPosition(center);
                infowindow.open(scope.map);
            }
        }
        return directive;
    }
})();
