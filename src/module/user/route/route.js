(function () {
    angular.module('user')
            .config(function ($routeProvider, $locationProvider) {
                $routeProvider
                        .when('/login', {
                            controller: 'LoginController',
                            templateUrl: '../module/user/views/login.html'
                        })
                        .when('/register', {
                            controller: 'RegisterController',
                            templateUrl: '../module/user/views/register.html'
                        })
                        .when('/forgotpassword', {
                            controller: 'ForgotPasswordController',
                            templateUrl: '../module/user/views/forgotpassword.html'
                        })
                        .when('/user/dashboard', {
                            controller: 'dashboardController',
                            templateUrl: '../module/user/views/userdashboard.html'
                        })
                        .when('/user/editprofile', {
                            controller: 'dashboardController',
                            templateUrl: '../module/user/views/editprofile.html'
                        })
                        .when('/user/changepassowrd', {
                            controller: 'dashboardController',
                            templateUrl: '../module/user/views/changepassword.html'
                        })
                        .when('/user/linktwitter', {
                            controller: 'dashboardController',
                            templateUrl: '../module/user/views/linktwitteraccount.html'
                        })
            });
})();
