//var _= require('lodash');
(function() {
    angular.module('user').controller('dashboardController', dashboardController);
    dashboardController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$window', '$sessionStorage', 'MessageService'];
    function dashboardController($scope, $rootScope, $location, UserService, $window, $sessionStorage, MessageService) {
        var vm = this;
        vm.dataLoading = false;
        vm.init = function() {
            var userId = $sessionStorage.loginData.id;
            UserService.getProfile(userId).then(function(userData)
            {
                vm.userdata = userData.data;
            });
        };
        vm.updateProfile = function() {
            vm.dataLoading = true;
            console.log(vm.userdata);
            UserService.updateProfile(vm.userdata).success(function(response, status)
            {
                if (status == 200) {
                    MessageService.success('Your profile updated successfully.');
                    response.password = '';
                    vm.userdata = response;
                } else
                {
                    MessageService.alert(response);
                }
                vm.dataLoading = false;
            });
        };
        vm.logout = function() {
            delete $sessionStorage.loginData;
            UserService.logout().success(function(response, status) {
                $window.location.href = '/';
            });
        };
    }
})();
