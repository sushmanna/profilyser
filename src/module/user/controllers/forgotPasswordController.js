(function() {

    angular.module('user').controller('ForgotPasswordController', ForgotPasswordController);
    ForgotPasswordController.$inject = ['$scope', '$rootScope', '$location', 'UserService','MessageService'];
    function ForgotPasswordController($scope, $rootScope, $location, UserService,MessageService) {
        var vm = this;
        vm.dataLoading = false;
        vm.form = {
            reply_to_display_name: 'Profilyser Inquery',
            //reply_to: 'biswarup.das@profilyser.com',
            reply_to: 'contact@profilyser.com',
            to_display_name: 'Profilyser',
        };
        vm.forgotpassword = function()
        {
             vm.dataLoading = true;
            UserService.forgotpassword(vm.form).then(function(response) {
                vm.dataLoading = false;
                if (response.status == 200) {
                    MessageService.success(response.data.message);
                    //vm.form.email = '';
                    
                } else
                {
                    MessageService.alert(response.data.message);
                }
               
            });
        };
    }

})();
