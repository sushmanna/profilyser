(function() {
    angular.module('user').controller('headerController', headerController);
    headerController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$window', '$sessionStorage'];

    function headerController($scope, $rootScope, $location, UserService, $window, $sessionStorage) {
        var vm = this;
        vm.islogin=false;
        
        vm.init = function() {
            if (angular.isDefined($sessionStorage.loginData))
            {
                console.log('true');
                vm.islogin=true;
            }
            else
            {console.log('false');
                vm.islogin=false;
            }
        };
        vm.dashboard = function() {
           UserService.loginRedirect($sessionStorage.loginData);
        };
        vm.logout = function() {
            delete $sessionStorage.loginData;
            $window.location.href = '/';
        };

        vm.isCurrentPath = function (path) {
            return $location.path() == path;
        };
        
    }

})();
