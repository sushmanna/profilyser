(function() {

    angular.module('user').controller('RegisterController', RegisterController);
    RegisterController.$inject = ['$scope', '$rootScope', '$location', 'UserService'];

    function RegisterController($scope, $rootScope, $location, UserService) {
        var vm = this;
        vm.message = '';
        vm.error = '';
        vm.dataLoading=false;
        /* function to create Member */
        vm.register = function() {
            //    data – {string|Object} – The response body transformed with the transform functions.
            //    status – {number} – HTTP status code of the response.
            //    headers – {function([headerName])} – Header getter function.
            //    config – {Object} – The configuration object that was used to generate the request.
            //    statusText – {string} – HTTP status text of the response.
            vm.dataLoading=true;
            UserService.register(vm.form).success(function(response, status) {
                if(status=='202')
                {
                    vm.error=response.errors;
                }
                else
                {
                    vm.error = '';
                    vm.message ="You have successfully registered."
                }
                vm.dataLoading=false;
            });
        };
    }

})();
