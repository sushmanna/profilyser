(function () {

    angular.module('user').controller('LoginController', LoginController);
    LoginController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$window', '$sessionStorage'];

    function LoginController($scope, $rootScope, $location, UserService, $window, $sessionStorage) {
        var vm = this;
        vm.error = '';
        vm.dataLoading = false;
        vm.login = function () {
            vm.dataLoading = true;
            UserService.login(vm.form).success(function (response, status) {
                if (status == '202')
                {
                    vm.error = 'Your username and password combination are invalid';
                }
                else
                {
                    $sessionStorage.loginData = response;
                    UserService.loginRedirect(response);
                }
                vm.dataLoading = false;
            });
        };
    }

})();
