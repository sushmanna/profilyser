(function() {
    angular.module('home')
            .config(function($routeProvider, $locationProvider) {
                $routeProvider
                        .when('/', {
                            controller: 'HomeController',
                            templateUrl: '../module/home/views/home.html'
                        })
                        .when('/cms/:cmsKey', {
                            controller: 'HomeController',
                            templateUrl: '../module/home/views/cms.html'
                        })
            });
})();
