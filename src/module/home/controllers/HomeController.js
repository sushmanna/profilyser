(function() {
    angular.module('home').controller('HomeController', HomeController);
    HomeController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$routeParams', 'AdminService','$window'];
    function HomeController($scope, $rootScope, $location, UserService, $routeParams, AdminService,$window) {
        var vm = this;
        vm.cmsContent = '';
        vm.disabled = false;
    vm.menu = [
        ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
        ['format-block'],
        ['font'],
        ['font-size'],
        ['remove-format'],
        ['ordered-list', 'unordered-list', 'outdent', 'indent'],
        ['left-justify', 'center-justify', 'right-justify'],
        ['code', 'quote', 'paragraph'],
        ['link', 'image'],
        ['css-class']
    ];

    vm.cssClasses = ['test1', 'test2'];

    vm.setDisabled = function() {
        vm.disabled = !vm.disabled;
    }
        vm.initcms = function() {
            var cmskey = $routeParams.cmsKey;
            AdminService.getCmsContent(cmskey).success(function(response, status)
            {
                if(status==202)
                {
                    $window.location.href='/';
                }
                vm.cmsContent = response;
            });
        };
    }
})();
