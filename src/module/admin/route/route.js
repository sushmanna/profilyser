(function () {
    angular.module('admin')
            .config(function ($routeProvider, $locationProvider) {
                $routeProvider
                        .when('/admin', {
                            controller: 'AdminDashboardController',
                            templateUrl: '../module/admin/views/dashboard.html'
                        })
                        .when('/admin/profile', {
                            controller: 'adminProfileController',
                            templateUrl: '../module/admin/views/profile.html'
                        })
                        .when('/admin/settings', {
                            controller: 'adminSettingsController',
                            templateUrl: '../module/admin/views/setting.html'
                        })
                        .when('/admin/listcms', {
                            controller: 'adminCmsController',
                            templateUrl: '../module/admin/views/listcms.html'
                        })
                        .when('/admin/editcms/:id', {
                            controller: 'adminCmsController',
                            templateUrl: '../module/admin/views/editcms.html'
                        })
                        .when('/admin/managecity', {
                            controller: 'adminManageCitiesController',
                            templateUrl: '../module/admin/views/addeditcity.html'
                        })
                        .when('/admin/managecity/:id', {
                            controller: 'adminManageCitiesController',
                            templateUrl: '../module/admin/views/addeditcity.html'
                        })
                        .when('/admin/listcity', {
                            controller: 'adminManageCitiesController',
                            templateUrl: '../module/admin/views/listcity.html'
                        })
                        .when('/admin/managecrawler', {
                            controller: 'adminManageCrawlerController',
                            templateUrl: '../module/admin/views/managecrawler.html'
                        })
            });
})();
