(function() {
    angular.module('admin').controller('adminSettingsController', adminSettingsController);
    adminSettingsController.$inject = ['$scope', '$rootScope', '$location', 'UserService','$sessionStorage','AdminService'];
    function adminSettingsController($scope, $rootScope, $location, UserService,$sessionStorage,AdminService) {
        var vm = this;
        vm.setting={};
        vm.dataLoading = false;
        vm.message = '';
        
        vm.init=function()
        {
            AdminService.getSettings($sessionStorage.loginData.id).then(function(response){
                vm.setting=response.data;
            });
        };
        vm.updateSettings=function()
        {
            vm.dataLoading = true;
            var Setdata={};
            Setdata.id=$sessionStorage.loginData.id;
            Setdata.data=vm.setting;
            AdminService.updateSettings(Setdata).then(function(response){
                vm.setting=response.data;
                vm.dataLoading = false;
                vm.message = 'All setting updated successfully.';
            });
        };
    }
})();
