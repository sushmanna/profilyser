(function() {
    angular.module('admin').controller('adminManageCrawlerController', adminManageCrawlerController);
    adminManageCrawlerController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$sessionStorage', 'AdminService'];
    function adminManageCrawlerController($scope, $rootScope, $location, UserService, $sessionStorage, AdminService) {
        var vm = this;
        vm.dataLoading = false;
        vm.message = '';
        vm.message1 = '';
        vm.error = '';
        vm.error1 = '';
        vm.status = '';

        vm.getSetting = function()
        {
            AdminService.getCrawlerDay($sessionStorage.loginData.id).then(function(response) {
                vm.setting = response.data;
            });
        };
        vm.updateSetting = function()
        {
            vm.dataLoading = true;
            var Setdata = {};
            Setdata.id = $sessionStorage.loginData.id;
            Setdata.data = vm.setting;
            AdminService.updateCrawlerDay(Setdata).then(function(response) {
                vm.setting = response.data;
                vm.dataLoading = false;
                vm.message = 'All setting updated successfully.';
            });
        };
        vm.deleteCrawlerData = function()
        {
            if (confirm("sure to delete")) {
                vm.dataLoading = true;
                AdminService.deleteCrawlerData($sessionStorage.loginData.id).then(function(response) {
                    vm.dataLoading = false;
                    vm.message1 = 'Delete Data Successfully';
                });
            }
        };
        vm.restartCrawler = function()
        {
            vm.dataLoading = true;
            AdminService.restartCrawler($sessionStorage.loginData.id).then(function(response) {
                vm.dataLoading = false;
                vm.message1 = 'Restart Crawler Successfully';
            });
        };
        vm.stopCrawler = function()
        {
            vm.dataLoading = true;
            AdminService.stopCrawler($sessionStorage.loginData.id).then(function(response) {
                vm.dataLoading = false;
                vm.message1 = 'Stop Crawler Successfully';
            });
        };

        vm.startCrawler = function()
        {
            vm.dataLoading = true;
            AdminService.startCrawler($sessionStorage.loginData.id).then(function(response) {
                vm.dataLoading = false;
                vm.message1 = 'Start Crawler Successfully';
            });
        };

//        vm.statusCrawler = function ()
//        {
//            vm.dataLoading = true;
//            AdminService.statusCrawler($sessionStorage.loginData.id).then(function (response) {
//                vm.dataLoading = false;
//                
//                
//                'crawler.js -monitor -production'
//                vm.status = response.data;
//            });
//        };
    }
})();
