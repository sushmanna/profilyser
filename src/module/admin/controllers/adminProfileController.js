(function() {
    angular.module('admin').controller('adminProfileController', adminProfileController);
    adminProfileController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$sessionStorage'];
    function adminProfileController($scope, $rootScope, $location, UserService, $sessionStorage) {
        var vm = this;
        vm.userdata = {};
        vm.dataLoading = false;
        vm.message = '';
        vm.init = function()
        {
            var userId = $sessionStorage.loginData.id;
            UserService.getProfile(userId).then(function(userData)
            {
                vm.userdata = userData.data;
            });
        };
        vm.updateProfile = function()
        {
            vm.dataLoading = true;
            UserService.updateProfile(vm.userdata).success(function(response, status) 
            {
                if (response == '202')
                {
                    vm.error = 'Profile updation failure.';
                }
                else
                {
                    vm.message = 'Your profile updated successfully.';
                    response.password = '';
                    vm.userdata = response;
                }
                vm.dataLoading = false;
            });
        };
    }
})();
