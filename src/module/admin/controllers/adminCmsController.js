(function () {
    angular.module('admin').controller('adminCmsController', adminCmsController);
    adminCmsController.$inject = ['$scope', '$rootScope', '$location', 'UserService', 'AdminService', '$sessionStorage', '$routeParams'];
    function adminCmsController($scope, $rootScope, $location, UserService, AdminService, $sessionStorage, $routeParams) {
        var vm = this;
        vm.cmsList = {};
        vm.dataLoading = false;
        vm.message = '';
        vm.cmsData = {};

        vm.initCmsList = function ()
        {
            AdminService.getCmsList($sessionStorage.loginData.id).then(function (response) {
                vm.cmsList = response.data;
            });
        };
        vm.editCms = function ()
        {
            AdminService.getCms($sessionStorage.loginData.id, $routeParams.id).then(function (response) {
                vm.cmsData = response.data;
            });
        };
        vm.updateCms = function ()
        {
            vm.dataLoading = true;
            AdminService.updateCms($sessionStorage.loginData.id, $routeParams.id, vm.cmsData).then(function (response) {
                vm.dataLoading = false;
                vm.message = 'Cms Page updated successfully.';
            });
        };

    }
})();
