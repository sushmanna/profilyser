(function () {
    angular.module('admin').controller('adminMainController', adminMainController);
    adminMainController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$sessionStorage', '$window'];
    function adminMainController($scope, $rootScope, $location, UserService, $sessionStorage, $window) {
        var vm = this;
        console.log('AdminHeaderController');
        vm.loginData = $sessionStorage.loginData
        vm.logout = function ()
        {
            delete $sessionStorage.loginData;
            UserService.logout().success(function (response, status) {
                $window.location.href = '/';
            });
        };
    }
})();
