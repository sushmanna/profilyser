(function () {
    angular.module('admin').controller('adminManageCitiesController', adminManageCitiesController);
    adminManageCitiesController.$inject = ['$scope', '$rootScope', '$location', 'UserService', '$sessionStorage', 'AdminService', '$routeParams', 'MessageService'];
    function adminManageCitiesController($scope, $rootScope, $location, UserService, $sessionStorage, AdminService, $routeParams, MessageService) {
        var vm = this;
        vm.cities = {};
        vm.dataLoading = false;
        vm.message = '';
        vm.error = '';
        vm.initCityList = function () {
            AdminService.getCityList().then(function (response) {
                vm.cities = response.data;
            });
        };
        vm.initCity = function () {
            var getID = $routeParams.id;
            if (getID != null)
            {
                AdminService.getCity(getID).then(function (response) {
                    vm.city = response.data;
                });
            }
        };
        vm.updateCreateCity = function () {
            vm.dataLoading = true;
            if (vm.city._id)
            {
                AdminService.updateCity(vm.city._id, vm.city).then(function (response) {
                    vm.dataLoading = false;
                    if (response.status == 200) {
                        MessageService.success('City updated Susccessfully');
                    } else
                    {
                        MessageService.alert(response.data);
                    }
                    vm.city = vm.city;

                });
            }
            else {

                AdminService.createCity(vm.city).then(function (response) {
                    vm.dataLoading = false;
                    if (response.status == 200) {
                        MessageService.success('City Created Susccessfully', 'admin/listcity');
                    } else
                    {
                        MessageService.alert(response.data);
                    }
                });
            }

        };
        vm.deleteCity = function (cityId) {
            AdminService.deteleCity(cityId).then(function (response) {
                vm.cities = response.data;
            });
        };
        vm.changeToDefaultCity = function (cityId) {
            AdminService.changeToDefaultCity(cityId).success(function (responseData, status) {
                if (status == '202')
                {
                    MessageService.alert('Default City updation failure.');
                }
                else
                {
                    vm.cities = responseData;
                    MessageService.success('Default City successfully changed');
                }
            });
        };
        vm.inactiveCrawler = function (cityId) {

            AdminService.changeCrawlerCityStatus(cityId, false).success(function (responseData, status) {


                if (status == '202')
                {
                    MessageService.alert(responseData);
                }
                else
                {

                    MessageService.success('City updated successfully.');
                    vm.cities = responseData;
                }
            });
        };
        vm.activeCrawler = function (cityId) {
            AdminService.changeCrawlerCityStatus(cityId, true).success(function (responseData, status) {
                if (status == '202')
                {
                    MessageService.alert(responseData);
                }
                else
                {
                    vm.cities = responseData;
                    MessageService.success('City updated successfully.');
                }
            });
        };


        vm.inactiveFreeAccess = function (cityId) {

            AdminService.changeCrawlerCityAccess(cityId, false).success(function (responseData, status) {


                if (status == '202')
                {
                    MessageService.alert(responseData);
                }
                else
                {

                    MessageService.success('City updated successfully.');
                    vm.cities = responseData;
                }
            });
        };
        vm.activeFreeAccess = function (cityId) {
            AdminService.changeCrawlerCityAccess(cityId, true).success(function (responseData, status) {
                if (status == '202')
                {
                    MessageService.alert(responseData);
                }
                else
                {
                    vm.cities = responseData;
                    MessageService.success('City updated successfully.');
                }
            });
        };
    }
})();
