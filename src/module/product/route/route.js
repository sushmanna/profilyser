(function() {
    angular.module('product').config(function($routeProvider, $locationProvider) {
        $routeProvider
                .when('/product', {
                    controller: 'ProductController',
                    templateUrl: '../module/product/views/product.html',
                    resolve: {
                        AllTweets: function($ngBootbox, $window) {
                            var promptOptions = {
                                title: "If you wish to test the product but do not have a password, please, <a href='/contact' target='_Self'>contact us</a> or Enter password",
                                inputType : 'password',
                                className :'product-box',
                                buttons: {
                                    confirm: {
                                        label: "Save"
                                    }
                                },
                                callback: function(result) {
                                    if (result === null) {
                                        console.log("Prompt dismissed");
                                        $window.location.href = '/';
                                    } else {
                                        if (result == 'profilyser@websummit')
                                        //if (result == '12')
                                        {
                                            return true;
                                        }
                                        else
                                        {
                                            $window.location.href = '/';
                                        }
                                    }
                                }
                            };
                            bootbox.prompt(promptOptions);
                        }
                    }
                })
                .when('/product/result', {
                    controller: 'ProductController',
                    templateUrl: '../module/product/views/result.html'
                })
                .when('/product/result1', {
                    controller: 'Product1Controller',
                    templateUrl: '../module/product/views/result1.html'
                })
                .when('/product/search', {
                    controller: 'SearchController',
                    controllerAs: 'vm',
                    templateUrl: '../module/product/views/search.html',
                    resolve: {
                        AllTweets: function(TwitterService, $rootScope) {
                            var tweet = TwitterService.getMapTweets();
                            $rootScope.mapData = tweet;
                            return tweet;
                        }
                    }

                })
    });
})();