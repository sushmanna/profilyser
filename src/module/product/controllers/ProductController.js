(function() {
    var twitter = require('twitter-text');
    centerlat = '';
    centerlon = '';
    angular.module('product').controller('ProductController', ProductController);
    ProductController.$inject = ['$scope', '$rootScope', '$location', '$q', '$sessionStorage', 'UserService', 'AdminService', 'TwitterService', 'MessageService'];

    window.onpageshow = function(event) {
        if (event.persisted) {
            window.location.reload();
        }
    };

    function ProductController($scope, $rootScope, $location, $q, $sessionStorage, UserService, AdminService, TwitterService, MessageService) {
        var vm = this;

        vm.heatMapData = [];
        vm.twitMapData = [];
        vm.heatMapCenter = [];
        vm.twitMapCenter = [];
        vm.cloudelems = [];
        vm.zoom = 5;
        $scope.$on('changeZoom', function(event, data) {
            vm.zoom = data;
        });
        $scope.$on('changeCenter', function(event, data) {
            vm.heatMapCenter = vm.twitMapCenter = data;
        });
        vm.fp = {};
        vm.bp = {};
        vm.pp = {};
        vm.map = '';
        vm.heatmap = '';
        vm.topfp = [];
        vm.topbp = [];
        vm.toppp = [];
        vm.topfptweets = [];
        vm.topbptweets = [];
        vm.toppptweets = [];
        vm.exportCsvHeader = ["Pseudonym", "Followers", "Friends", "Statuses_count", "Influence score", "Profile URL"];
        vm.preloader = false;
        
        if (!$sessionStorage.loginData) {
            vm.cityparam = {
                allow_free_access: true
            };
        }

        vm.cityoption = [];
        vm.twite = {};
        vm.exportActive = false;
        vm.tabs = [];
        vm.dateOptions = {
            formatYear: 'yy',
        };
        vm.fromDateStatus = {
            opened: false
        };
        vm.toDateStatus = {
            opened: false
        };
        vm.openFromDate = function($event) {
            vm.fromDateStatus.opened = true;
        };
        vm.openToDate = function($event) {
            vm.toDateStatus.opened = true;
        };
        vm.today = new Date();
        vm.twite.raw_to_date = vm.today;
        vm.format = 'dd-MM-yyyy';

        vm.scrollbarConfig = {
            advanced: {
                updateOnContentResize: true
            },
            theme: 'dark'
        };
        vm.selectedUser = [];
        vm.showmap = 'heat';

        vm.twite.search_tags_arr=[];
        vm.splitTag = function(tags, $tag) {
            var ind = $tag.text.indexOf('-');
            if (ind != -1)
            {
                vm.twite.search_tags_arr.pop();
                var unique = [];
                 var arr=$tag.text.split("-"); 
                $.each(arr, function(index, word) {
                    if ($.inArray(word, unique) === -1)
                    {
                        unique.push(word);
                        var ele = {text: word};
                        vm.twite.search_tags_arr.push(ele);
                    }

                });
            }
        };
        vm.search = function() {
            vm.preloader = true;
            vm.fp = {};
            vm.bp = {};
            vm.pp = {};
            vm.map = '';
            vm.heatmap = '';
            vm.topfp = [];
            vm.topbp = [];
            vm.toppp = [];
            vm.topfptweets = [];
            vm.topbptweets = [];
            vm.toppptweets = [];
            vm.tabs = [];
            vm.twitMapData = [];
            vm.heatMapCenter = [];
            vm.cloudelems = [];
            
            vm.mapload = {
                famous: {
                    tweet: false,
                    heat: false
                },
                balanced: {
                    tweet: false,
                    heat: false
                },
                promising: {
                    tweet: false,
                    heat: false
                }
            };
            vm.showmap = 'heat';

            if(vm.twite.to_city.toLowerCase() == 'paris')
                vm.zoom = 8;
            else if(vm.twite.to_city.toLowerCase() == 'mayenne')
                vm.zoom = 7;
            else
                vm.zoom = 5;


            if (vm.twite.raw_from_date && vm.twite.raw_to_date) {
                vm.twite.from_date = new Date(vm.twite.raw_from_date).getFullYear() + '-' + (new Date(vm.twite.raw_from_date).getMonth() + 1) + '-' + new Date(vm.twite.raw_from_date).getDate() + ' 00:00:00';
                vm.twite.to_date = new Date(vm.twite.raw_to_date).getFullYear() + '-' + (new Date(vm.twite.raw_to_date).getMonth() + 1) + '-' + new Date(vm.twite.raw_to_date).getDate() + ' 23:59:59';
            }
            if (vm.twite.search_tags_arr && vm.twite.search_tags_arr.length > 0) {
                var tags = [];
                for (c in vm.twite.search_tags_arr) {
                    tags[c] = vm.twite.search_tags_arr[c].text;
                }
                //delete vm.twite.search_tags_arr;
                vm.twite.search_tags = tags.toString();
                console.log(vm.twite);
            } else {
                delete vm.twite.search_tags;
            }

            var process = [];
            process[0] = TwitterService.getFamousProfiles(vm.twite);
            process[1] = TwitterService.getBalancedProfiles(vm.twite);
            process[2] = TwitterService.getPromisingProfiles(vm.twite);
            process[3] = TwitterService.getMapTweets(vm.twite);
            process[4] = AdminService.getCityByQuery(vm.twite);
            $q.all(process).then(function(res) {
                if (res[0].data.length > 0 && res[1].data.length > 0 && res[2].data.length > 0) {
                    vm.fp = res[0].data;
                    for (var i = 0; i < res[0].data.length; i++) {
                        if (i < 3) {
                            vm.topfp.push(res[0].data[i]._id.twitter_user_id);
                        }
                    }
                    /*vm.topfp.push(res[0].data[0]._id.twitter_user_id);
                     vm.topfp.push(res[0].data[1]._id.twitter_user_id);
                     vm.topfp.push(res[0].data[2]._id.twitter_user_id);*/
                    var CsvFpData = [];
                    for (c in vm.fp) {
                        var element = [];
                        element[0] = vm.fp[c].full_name;
                        element[1] = vm.fp[c].follower_number;
                        element[2] = vm.fp[c].followees_number;
                        element[3] = vm.fp[c].tweet_count;
                        element[4] = vm.fp[c].point > 0 ? vm.fp[c].point : 0;
                        element[5] = 'http://twitter.com/' + vm.fp[c].screen_name;
                        CsvFpData.push(element);
                    }
                    vm.exportFpCsv = CsvFpData;
                    vm.bp = res[1].data;
                    for (var i = 0; i < res[1].data.length; i++) {
                        if (i < 3) {
                            vm.topbp.push(res[1].data[i]._id.twitter_user_id);
                        }
                    }
                    /*vm.topbp.push(res[1].data[0]._id.twitter_user_id);
                     vm.topbp.push(res[1].data[1]._id.twitter_user_id);
                     vm.topbp.push(res[1].data[2]._id.twitter_user_id);*/
                    var CsvBpData = [];
                    for (c in vm.bp) {
                        var element = [];
                        element[0] = vm.bp[c].full_name;
                        element[1] = vm.bp[c].follower_number;
                        element[2] = vm.bp[c].followees_number;
                        element[3] = vm.bp[c].tweet_count;
                        element[4] = vm.bp[c].point >= 1 ? Math.log(vm.bp[c].point).toFixed(2) : 0;
                        element[5] = 'http://twitter.com/' + vm.bp[c].screen_name;
                        CsvBpData.push(element);
                    }
                    vm.exportBpCsv = CsvBpData;
                    vm.pp = res[2].data;
                    for (var i = 0; i < res[2].data.length; i++) {
                        if (i < 3) {
                            vm.toppp.push(res[2].data[i]._id.twitter_user_id);
                        }
                    }
                    /*vm.toppp.push(res[2].data[0]._id.twitter_user_id);
                     vm.toppp.push(res[2].data[1]._id.twitter_user_id);
                     vm.toppp.push(res[2].data[2]._id.twitter_user_id);*/
                    var CsvPpData = [];
                    for (c in vm.pp) {
                        var element = [];
                        element[0] = vm.pp[c].full_name;
                        element[1] = vm.pp[c].follower_number;
                        element[2] = vm.pp[c].followees_number;
                        element[3] = vm.pp[c].tweet_count;
                        element[4] = vm.pp[c].point >= 1 ? (Math.log(vm.pp[c].point) * 100).toFixed(2) : 0;
                        element[5] = 'http://twitter.com/' + vm.pp[c].screen_name;
                        CsvPpData.push(element);
                    }
                    vm.exportPpCsv = CsvPpData;
                    /*for (i in res[3].data) {
                     vm.heatMapData.push({latlon: [res[3].data[i].latitude, res[3].data[i].longitude]});
                     }
                     
                     vm.heatMapCenter = [res[3].data[0].latitude, res[3].data[0].longitude];*/
                }

                if (res[3].data.data.length > 0) {
                    vm.twitMapData = res[3].data.data;
                    vm.heatMapData = res[3].data.data;

                    var hashtags = [];
                    for (i in vm.twitMapData) {
                        hashtags = hashtags.concat(twttr.txt.extractHashtags(vm.twitMapData[i].tweet_content));
                        vm.twitMapData[i].match = res[3].data.matchtweetcount[vm.twitMapData[i].user_id];
                        vm.twitMapData[i].tweet_content = twitter.autoLinkUrlsCustom(twitter.htmlEscape(vm.twitMapData[i].tweet_content), {targetBlank: 1});
                    }

                    var obj = {};
                    for (var i = 0, j = hashtags.length; i < j; i++) {
                        obj[hashtags[i].toLowerCase()] = (obj[hashtags[i].toLowerCase()] || 0) + 1;
                    }
                    var sortable = [];
                    var keywordnotexist = [];
                    if (vm.twite.search_tags)
                        keywordnotexist = keywordnotexist.concat(vm.twite.search_tags.split(","));
                    if (vm.twite.to_city)
                        keywordnotexist.push(vm.twite.to_city);
                    for (var i in keywordnotexist) {
                        keywordnotexist[i] = keywordnotexist[i].toLowerCase();
                    }
                    //console.log(keywordnotexist);
                    for (var tag in obj) {
                        if (keywordnotexist.indexOf(tag) == -1 && tag.length > 3) {
                            sortable.push({
                                tag: tag,
                                count: obj[tag]
                            });
                        }
                    }
                    //console.log(sortable);   
                    sortable.sort(function(a, b) {
                        return b.count - a.count
                    });
                    for (var i = 0; i < sortable.length; i++) {
                        if (i > 14)
                            break;
                        var ele = {};
                        //ele.text = sortable[i].tag+'('+sortable[i].count+')';
                        ele.text = sortable[i].tag;
                        //ele.id = sortable[i].count;
                        ele.weight = 1.0;
                        if (i <= 4)
                            ele.bgcolor = "#FC8801";
                        if (i > 4 && i <= 9)
                            ele.bgcolor = "#0C183E";
                        if (i > 9 && i < 15)
                            ele.bgcolor = "#54ACEC";
                        vm.cloudelems.push(ele);
                    }
                    ;
                }
                if (res[4].data.length > 0) {
                    var west_longitude = parseFloat(res[4].data[0].west_longitude);
                    var south_latitude = parseFloat(res[4].data[0].south_latitude);
                    var east_longitude = parseFloat(res[4].data[0].east_longitude);
                    var north_latitude = parseFloat(res[4].data[0].north_latitude);
                    centerlat = (south_latitude + north_latitude) / 2;
                    centerlon = (west_longitude + east_longitude) / 2;
                    console.log(centerlat);
                    console.log(centerlon);
                    vm.heatMapCenter = [centerlat, centerlon];
                    vm.twitMapCenter = [centerlat, centerlon];
                }
            }).then(function(res2) {
                var processusers = [];
                for (i in vm.topfp) {
                    processusers.push(TwitterService.getTweets(vm.twite, {
                        user_id: vm.topfp[i]
                    }));
                }
                for (i in vm.topbp) {
                    processusers.push(TwitterService.getTweets(vm.twite, {
                        user_id: vm.topbp[i]
                    }));
                }
                for (i in vm.toppp) {
                    processusers.push(TwitterService.getTweets(vm.twite, {
                        user_id: vm.toppp[i]
                    }));
                }
                /*processusers[0] = TwitterService.getTweets({user_id: vm.topfp});
                 processusers[1] = TwitterService.getTweets({user_id: vm.topbp});
                 processusers[2] = TwitterService.getTweets({user_id: vm.toppp});*/
                $q.all(processusers).then(function(res3) {
                    var cnt = 0;
                    for (var i = 0; i < vm.topfp.length; i++) {
                        vm.topfptweets.push(res3[cnt].data);
                        cnt++;
                    }
                    for (var i = 0; i < vm.topbp.length; i++) {
                        vm.topbptweets.push(res3[cnt].data);
                        cnt++;
                    }
                    for (var i = 0; i < vm.toppp.length; i++) {
                        vm.toppptweets.push(res3[cnt].data);
                        cnt++;
                    }
                    vm.tabs = [{
                            title: '<i class="fa fa-star"></i> Famous',
                            data: vm.fp,
                            csvheader: vm.exportCsvHeader,
                            csvdata: vm.exportFpCsv,
                            filename: 'FAMOUS-PROFILES.csv',
                            toppro: vm.topfptweets,
                            counter: 0,
                            //twitMapCenter: vm.twitMapCenter,
                            twitMapData: vm.twitMapData,
                            //heatMapCenter: vm.heatMapCenter,
                            heatMapData: vm.heatMapData
                        }, {
                            title: '<i class="fa fa-balance-scale"></i> Balanced',
                            data: vm.bp,
                            csvheader: vm.exportCsvHeader,
                            csvdata: vm.exportBpCsv,
                            filename: 'BALANCED-PROFILES.csv',
                            toppro: vm.topbptweets,
                            counter: 1,
                            //twitMapCenter: vm.twitMapCenter,
                            twitMapData: vm.twitMapData,
                            //heatMapCenter: vm.heatMapCenter,
                            heatMapData: vm.heatMapData
                        }, {
                            title: '<i class="fa fa-line-chart"></i> Promising',
                            data: vm.pp,
                            csvheader: vm.exportCsvHeader,
                            csvdata: vm.exportPpCsv,
                            filename: 'PROMISING-PROFILES.csv',
                            toppro: vm.toppptweets,
                            counter: 2,
                            //twitMapCenter: vm.twitMapCenter,
                            twitMapData: vm.twitMapData,
                            //heatMapCenter: vm.heatMapCenter,
                            heatMapData: vm.heatMapData
                        }];
                    vm.preloader = false;
                });
            });
        };
        vm.tabSelectCallback = function(tab) {
            if (vm.tabs) {
                vm.tabs[0].twitMapData = vm.twitMapData;
                vm.tabs[1].twitMapData = vm.twitMapData;
                vm.tabs[2].twitMapData = vm.twitMapData;
            }
            vm.selectedUser = [];
            vm.mapload = {
                famous: {
                    tweet: false,
                    heat: false
                },
                balanced: {
                    tweet: false,
                    heat: false
                },
                promising: {
                    tweet: false,
                    heat: false
                }
            };
            if (tab.counter == 0) {
                vm.mapload.famous[vm.showmap] = true;
            } else if (tab.counter == 1) {
                vm.mapload.balanced[vm.showmap] = true;
            } else {
                vm.mapload.promising[vm.showmap] = true;
            }
        };
        vm.callbackMapToggle = function(map, tab) {
            if (vm.tabs) {
                vm.tabs[0].twitMapData = vm.twitMapData;
                vm.tabs[1].twitMapData = vm.twitMapData;
                vm.tabs[2].twitMapData = vm.twitMapData;
            }
            vm.selectedUser = [];
            vm.showmap = map;
            //vm.mapload = { famous: { tweet: false, heat: false}, balanced: { tweet: false, heat: false}, promising: { tweet: false, heat: false}};
            if (tab.counter == 0) {
                if (map == 'tweet')
                    vm.mapLoad('famous.tweet');
                //vm.mapload.famous.tweet = true;
                if (map == 'heat')
                    vm.mapLoad('famous.heat');
                //vm.mapload.famous.heat = true;
            } else if (tab.counter == 1) {
                if (map == 'tweet')
                    vm.mapLoad('balanced.tweet');
                //vm.mapload.balanced.tweet = true;   
                if (map == 'heat')
                    vm.mapLoad('balanced.heat');
                //vm.mapload.balanced.heat = true;
            } else {
                if (map == 'tweet')
                    vm.mapLoad('promising.tweet');
                // vm.mapload.promising.tweet = true;
                if (map == 'heat')
                    vm.mapLoad('promising.heat');
                //vm.mapload.promising.heat = true;
            }
        };
        vm.callbackListItem = function(user_id, counter, index) {
            var params = JSON.parse(JSON.stringify(vm.twite));
            vm.showmap = 'tweet';
            if( vm.selectedUser.indexOf(user_id) != -1){
                vm.selectedUser.splice( vm.selectedUser.indexOf(user_id),1);
            } else {
                vm.selectedUser.push(user_id);
            }

            if (counter == 0) {
                vm.mapload.famous.tweet = false;
            } else if (counter == 1) {
                vm.mapload.balanced.tweet = false;
            } else {
                vm.mapload.promising.tweet = false;
            }

            if (vm.selectedUser.length == 0 && vm.tabs) {
                vm.callbackMapToggle('heat', vm.tabs[counter]);
                return;
            }

            var processtwitters = [];
            var myparams;

            for (var i in vm.selectedUser) {
                myparams = {user_id: vm.selectedUser[i]};

                if (params.raw_to_date)
                    myparams.raw_to_date = params.raw_to_date;
                if (params.raw_from_date)
                    myparams.raw_from_date = params.raw_from_date;
                if (params.to_city)
                    myparams.to_city = params.to_city;
                if (params.search_tags_arr)
                    myparams.search_tags_arr = params.search_tags_arr;
                if (params.search_tags)
                    myparams.search_tags = params.search_tags;
                if (params.from_date)
                    myparams.from_date = params.from_date;
                if (params.to_date)
                    myparams.to_date = params.to_date;

                processtwitters.push(TwitterService.getMapTweets(myparams));
            }



            $q.all(processtwitters).then(function(response) {
                var res = [];
                for (var i in response) {
                    var sd = response[i].data.data;
                    var sm = response[i].data.matchtweetcount;
                    if (sd.length > 0) {
                        for (var j in sd) {
                            sd[j].match = sm[sd[j].user_id];
                            sd[j].tweet_content = twitter.autoLinkUrlsCustom(twitter.htmlEscape(sd[j].tweet_content),{targetBlank:1});
                        }
                    }
                    res = res.concat(sd);
                }

                if (counter == 0) {
                    vm.tabs[counter].twitMapData = res;
                    //vm.mapload.famous.tweet = true;
                    vm.mapLoad('famous.tweet');
                } else if (counter == 1) {
                    vm.tabs[counter].twitMapData = res;
                    //vm.mapload.balanced.tweet = true;
                    vm.mapLoad('balanced.tweet');
                } else {
                    vm.tabs[counter].twitMapData = res;
                    //vm.mapload.promising.tweet = true;
                    vm.mapLoad('promising.tweet');
                }
                vm.heatMapCenter = [centerlat, centerlon];
                vm.twitMapCenter = [centerlat, centerlon];
            });
        };
        vm.mapLoad = function(activeMap) {
            vm.mapload = {
                famous: {
                    tweet: false,
                    heat: false
                },
                balanced: {
                    tweet: false,
                    heat: false
                },
                promising: {
                    tweet: false,
                    heat: false
                }
            };
            if (activeMap == 'famous.tweet') {
                vm.mapload.famous.tweet = true;
            }
            if (activeMap == 'balanced.tweet') {
                vm.mapload.balanced.tweet = true;
            }
            if (activeMap == 'promising.tweet') {
                vm.mapload.promising.tweet = true;
            }
            if (activeMap == 'famous.heat') {
                vm.mapload.famous.heat = true;
            }
            if (activeMap == 'balanced.heat') {
                vm.mapload.balanced.heat = true;
            }
            if (activeMap == 'promising.heat') {
                vm.mapload.promising.heat = true;
            }
        }
        vm.init = function() {
            AdminService.getCityByQuery(vm.cityparam).success(function(response, status) {
                vm.cityoption = response;
            });
        };

    }
})();