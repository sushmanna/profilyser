(function() {
    angular.module('product').controller('SearchController', SearchController);
    SearchController.$inject = ['$scope', '$rootScope', '$location', '$q', 'TwitterService'];
    function SearchController($scope, $rootScope, $location, $q, TwitterService) {
        var vm = this;

        $scope.$on('mapInitialized', function(event, evtMap) {
            $scope.$broadcast('setTweeettmap', {map: evtMap, marker: evtMap.markers[0]});
        });

        var west_longitude = -6.44;
        var south_latitude = 53.22;
        var east_longitude = -6.04;
        var north_latitude = 53.42;
        var centerlat = (south_latitude + north_latitude)/2;
        var centerlon = (west_longitude + east_longitude)/2;
        
        vm.heatMapData = [];
        vm.twitMapData = [];
        vm.heatMapCenter = [centerlat, centerlon];
        vm.twitMapCenter = [centerlat, centerlon];

        vm.fp = {};
        vm.bp = {};
        vm.pp = {};
        vm.map = '';
        vm.heatmap = '';

        vm.topfp = '';
        vm.topbp = '';
        vm.toppp = '';

        vm.topfptweets = {};
        vm.topbptweets = {};
        vm.toppptweets = {};
        vm.exportCsvHEader = ["Pseudonym", "Followers", "Friends", "Statuses_count", "Influence score", "Profile URL"];

        vm.initSearch = function() {
            var process = [];
            process[0] = TwitterService.getFamousProfiles();
            process[1] = TwitterService.getBalancedProfiles();
            process[2] = TwitterService.getPromisingProfiles();
            process[3] = TwitterService.getMapTweets();

            $q.all(process).then(function(res) {
                vm.fp = res[0].data;
                vm.topfp = res[0].data[0]._id.twitter_user_id;

                var CsvFpData = [];
                for (c in vm.fp)
                {
                    var element = [];
                    element[0] = vm.fp[c].full_name;
                    element[1] = vm.fp[c].follower_number;
                    element[2] = vm.fp[c].followees_number;
                    element[3] = vm.fp[c].tweet_count;
                    element[4] = vm.fp[c].point;
                    element[5] = 'http://twitter.com/' + vm.fp[c].screen_name;
                    CsvFpData.push(element);
                }
                vm.exportFpCsv = CsvFpData;


                vm.bp = res[1].data;
                vm.topbp = res[1].data[0]._id.twitter_user_id;

                var CsvBpData = [];
                for (c in vm.fp)
                {
                    var element = [];
                    element[0] = vm.bp[c].full_name;
                    element[1] = vm.bp[c].follower_number;
                    element[2] = vm.bp[c].followees_number;
                    element[3] = vm.bp[c].tweet_count;
                    element[4] = Math.log(vm.bp[c].point).toFixed(2);
                    element[5] = 'http://twitter.com/' + vm.bp[c].screen_name;
                    CsvBpData.push(element);
                }
                vm.exportBpCsv = CsvBpData;

                vm.pp = res[2].data;
                vm.toppp = res[2].data[0]._id.twitter_user_id;

                var CsvPpData = [];
                for (c in vm.pp)
                {
                    var element = [];
                    element[0] = vm.pp[c].full_name;
                    element[1] = vm.pp[c].follower_number;
                    element[2] = vm.pp[c].followees_number;
                    element[3] = vm.pp[c].tweet_count;
                    element[4] = Math.log(vm.pp[c].point).toFixed(2);
                    element[5] = 'http://twitter.com/' + vm.pp[c].screen_name;
                    CsvPpData.push(element);
                }
                vm.exportPpCsv = CsvPpData;


                for (i in res[3].data) {
                    vm.heatMapData.push({latlon: [res[3].data[i].latitude, res[3].data[i].longitude],fulldata:res[3].data[i]});
                }
                //console.log(vm.heatMapData);
                vm.heatMapCenter = [res[3].data[0].latitude, res[3].data[0].longitude];

            }).then(function(res2) {
                var processusers = [];
                processusers[0] = TwitterService.getTweets({},{user_id: vm.topfp});
                processusers[1] = TwitterService.getTweets({},{user_id: vm.topbp});
                processusers[2] = TwitterService.getTweets({},{user_id: vm.toppp});

                $q.all(processusers).then(function(res3) {
                    vm.topfptweets = res3[0].data;
                    vm.topbptweets = res3[1].data;
                    vm.toppptweets = res3[2].data;
                });
            });

        };

        //for cloud 
        vm.initTags = function() {
            var w = 960, h = 268;
            var clouder = document.getElementById('clouder');

            clouder.style.width = w * 2 / 3 + 'px';
            clouder.style.height = h * 2 / 3 + 'px';
            clouder.style.position = "absolute";
            clouder.style.left = w / 6 + 'px';
            clouder.style.top = h / 6 + 'px';

            window.clouder = new Clouder({
                container: clouder,
                tags: vm.createTags()
            });
        };
        vm.createTags = function() {
            var elems = [];
            elems.push({text: "Adwords", id: "1", weight: 0.1, bgcolor: "#FC8801", weight: 0.5});
            elems.push({text: "Engines", id: "2", weight: 0.1, bgcolor: "#FC8801", weight: 1.0});
            elems.push({text: "Important Keyword", id: "3", weight: 0.1, bgcolor: "#FC8801", weight: 1.0});
            elems.push({text: "Tweets", id: "4", weight: 0.1, bgcolor: "#FC8801", weight: 0.1});
            elems.push({text: "Relevant", id: "5", weight: 0.1, bgcolor: "#54ACEC", weight: 0.5});
            elems.push({text: "Making Money", id: "6", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Research", id: "7", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Edit", id: "8", weight: 0.1, bgcolor: "#FC8801", weight: 0.1});
            elems.push({text: "Competitive", id: "9", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Examples", id: "10", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Tools", id: "11", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Ranks", id: "12", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Web", id: "13", weight: 0.1, bgcolor: "#54ACEC", weight: 0.1});
            elems.push({text: "Word", id: "14", weight: 0.1, bgcolor: "#0C183E", weight: 0.1});
            elems.push({text: "Google", id: "15", weight: 0.1, bgcolor: "#54ACEC", weight: 0.1});
            return elems;
        };
        

        
    }
})();
