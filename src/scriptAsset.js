define(function (require, exports, module) {
    //sapatrate lib and icon
    require('file?name=bower_components/jquery/dist/jquery.min.js!./bower_components/jquery/dist/jquery.min.js');
    require('file?name=bower_components/bootbox/bootbox.js!./bower_components/bootbox/bootbox.js');
    require('file?name=bower_components/ngBootbox/dist/ngBootbox.min.js!./bower_components/ngBootbox/dist/ngBootbox.min.js');
    require('file?name=bower_components/moment/moment.js!./bower_components/moment/moment.js');
    require('file?name=bower_components/angular-moment/angular-moment.min.js!./bower_components/angular-moment/angular-moment.min.js');
    require('file?name=bower_components/ngmap/build/scripts/ng-map.min.js!./bower_components/ngmap/build/scripts/ng-map.min.js');
    require('file?name=bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js!./bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');
    require('file?name=js/clouder.js!./js/clouder.js');
    require('file?name=js/scripts.js!./js/scripts.js');
    require('file?name=js/global-constant.js!../config/global-constant.js');
    require('file?name=favicon.ico!./favicon.ico');

    //js
    require("./bower_components/ngstorage/ngStorage.js");
    require("./bower_components/angular-route/angular-route.min.js");
    require("./bower_components/angular-resource/angular-resource.min.js");
    require("./bower_components/angular-messages/angular-messages.min.js");
    require("./bower_components/angular-sanitize/angular-sanitize.min.js");
    require("./bower_components/ng-csv/build/ng-csv.min.js");
    require("./bower_components/bootstrap/dist/js/bootstrap.min.js");
    require("./bower_components/angular-bootstrap/ui-bootstrap.min.js");
    require("./bower_components/ng-tags-input/ng-tags-input.min.js");
    require("./bower_components/ng-scrollbars/dist/scrollbars.min.js");
    require("./bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js");
    require("./bower_components/angular-wysiwyg/dist/angular-wysiwyg.min.js");
    require("./module/app.js");


    //layout html
    require('./adminLayout.html');
    require('./index.html');


    //element
    require('./elements/admin_header.html');
    require('./elements/admin_sidebar.html');
    require('./elements/header.html');
    require('./elements/footer.html');
    require('./elements/user_sidebar.html'); 
    require('./elements/user_topbar.html');
    
    //tab
    require('./template/tabs/tab.html');
    require('./template/tabs/tabset.html');
    require('./template/datepicker/datepicker.html');
    require('./template/datepicker/popup.html');
    require('./template/datepicker/day.html');
    require('./template/datepicker/month.html');
    require('./template/datepicker/year.html');

    //load Service
    require('./module/Service/AdminService.js');
    require('./module/Service/UserService.js');
    require('./module/Service/MessageService.js');
    require('./module/Service/TwitterService.js');

    //load filter


    //load dirtective and template
    require('./module/Directives/heatMapDirective.js');
    require('./module/Directives/tweetMapDirective.js');
    require('./module/Directives/cloudDirective.js');
    //template
    require('./module/Directives/templates/heatMap.html');
    require('./module/Directives/templates/tweetMap.html');
    require('./module/Directives/templates/cloud.html');

    //load filters
    require('./module/Filters/allFilter.js');
        
    //home module
    require('./module/home/route/route.js');
    require('./module/home/controllers/HomeController.js');
    require('./module/home/views/home.html');
    require('./module/home/views/cms.html');

    //product module
    require('./module/product/route/route.js');
    require('./module/product/controllers/ProductController.js');
    require('./module/product/controllers/Product1Controller.js');
    require('./module/product/controllers/SearchController.js');
    require('./module/product/views/product.html');
    require('./module/product/views/result.html');
    require('./module/product/views/result1.html');
    require('./module/product/views/search.html');

    //demo module
    require('./module/demo/route/route.js');
    require('./module/demo/controllers/DemoController.js');
    require('./module/demo/views/demo.html');

    //contact module
    require('./module/contact/route/route.js');
    require('./module/contact/controllers/ContactController.js');
    require('./module/contact/views/contact.html');



    //user module element controller
    require('./module/user/controllers/headerController.js');

    //user module
    require('./module/user/route/route.js');
    require('./module/user/controllers/loginController.js');
    require('./module/user/controllers/registerController.js');
    require('./module/user/controllers/forgotPasswordController.js');
    require('./module/user/controllers/dashboardController.js');
    require('./module/user/controllers/profileController.js');
    require('./module/user/controllers/headerController.js');

    require('./module/user/views/login.html');
    require('./module/user/views/register.html');
    require('./module/user/views/forgotpassword.html');
    require('./module/user/views/userdashboard.html');
    require('./module/user/views/profile.html'); 
    require('./module/user/views/changepassword.html');
    require('./module/user/views/editprofile.html');
    require('./module/user/views/linktwitteraccount.html'); 
    //user module element controller

    //admin module
    require('./module/admin/route/route.js');
    require('./module/admin/controllers/adminMainController.js');
    require('./module/admin/controllers/AdminDashboardController.js');
    require('./module/admin/controllers/adminProfileController.js');
    require('./module/admin/controllers/adminSettingsController.js');
    require('./module/admin/controllers/adminCmsController.js');
    require('./module/admin/controllers/adminManageCitiesController.js');
    require('./module/admin/controllers/adminManageCrawlerController.js');

    require('./module/admin/views/dashboard.html');
    require('./module/admin/views/profile.html');
    require('./module/admin/views/setting.html');
    require('./module/admin/views/editcms.html');
    require('./module/admin/views/listcms.html');
    require('./module/admin/views/addeditcity.html');
    require('./module/admin/views/listcity.html');
    require('./module/admin/views/managecrawler.html');

});
