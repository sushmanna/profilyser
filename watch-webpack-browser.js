var webpack = require("webpack");
var config = require("./webpack.config.js");
var exec = require("child_process").exec;
var path = require('path');
var cwd = process.cwd();

// returns a Compiler instance
var compiler = webpack(config);

compiler.watch(200, function(err, stats) {
    console.time("Application prepare time");
    if (err) {
        console.log("fatal errors: " + err);
        return;
    }
    var jsonStats = stats.toJson();
    if (jsonStats.errors.length > 0) {
        console.log("errors: " + jsonStats.errors);
    }
    if (jsonStats.warnings.length > 0) {
        console.log('warnings: ' + jsonStats.warnings);
    }

    console.log(stats.toString());
    console.timeEnd("Application prepare time");
});


