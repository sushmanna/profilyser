var Twit = require('twit');
var mongoose = require('mongoose');
var $q = require('q');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var fs = require('fs');
var schedule = require('node-schedule');

var process = [];
//connect to local mongodb database
var db = mongoose.connect('mongodb://localhost/sensor');
//attach lister to connected event
mongoose.connection.once('connected', function(){
    console.log("Connected to database");
});


//get and intialized the model
var setting = require('./server/models/setting');
var City = require('./server/models/city');
var twitter = require('./server/models/tweet');

var CityModel = mongoose.model('City');
var SettingModel = mongoose.model('Setting');
var TwitterModel = mongoose.model('Twitter');

///setup process
process[0] = CityModel.find({}).where('isCrawler').equals(true).exec();
process[1] = SettingModel.where('key').in(['consumer_key', 'consumer_secret', 'access_token_key', 'access_token_secret']).exec();

//excute the process
$q.all(process).then(function(result){
    for (qa in result[0]) {
        var counter = qa;
        //get the latitute and longitute of city
        var latnlon = result[0][qa].west_longitude + ',' + result[0][qa].south_latitude + ',' + result[0][qa].east_longitude + ',' + result[0][qa].north_latitude;
        var params = {locations: latnlon};
        var twitterDetails = {
            consumer_key: result[0][qa].consumer_key,
            consumer_secret: result[0][qa].consumer_secret,
            access_token: result[0][qa].access_token_key,
            access_token_secret: result[0][qa].access_token_secret
        };
        var client = new Twit(twitterDetails);
        var stream = client.stream('statuses/filter', params);
        streamFunction(stream, counter, result[0][counter]);
    }

});
//var exec1 = require('child_process').exec;

function streamFunction(object, counter, result){
    object.on('connect', function(request){
        writeFile('Connected to Twitter API for city ' + result.name.toLowerCase() +' at '+ new Date());
    });
    object.on('disconnect', function(message){
        writeFile('Disconnected from Twitter API. Message: ' + message + 'for city ' + result.name.toLowerCase());
        sendreportmail('Crawler Disconnect report for city ' + result.name.toLowerCase(), 'Crawler Disconnectd for city ' + result.name.toLowerCase() + 'at' + new Date());
//        exec1('forever restart crawler-production.js -monitor -production', function(err, stdout, stderr){
//            writeFile('Crawler restated at' + new Date());
//            sendreportmail('Crawler restated at' + new Date(), 'Crawler restated at' + new Date());
//        });
    });
    object.on('reconnect', function(request, response, connectInterval){
        writeFile('Trying to reconnect to Twitter API in ' + connectInterval + ' ms for city ' + result.name.toLowerCase());
    });
    object.on('tweet', function(tweet){
        if(tweet.id){
            var tweetData = new TwitterModel({
                twitter_id: tweet.id,
                tweet_content: tweet.text,
                latitude: (tweet.coordinates === null) ? null : tweet.coordinates.coordinates[1],
                longitude: (tweet.coordinates === null) ? null : tweet.coordinates.coordinates[0],
                city: result.name.toLowerCase(),
                time_of_publication: tweet.timestamp_ms,
                user_id: tweet.user.id,
                screen_name: tweet.user.screen_name,
                profile_url: 'https://twitter.com/intent/user?user_id=' + tweet.user.id,
                friend_number: tweet.user.friends_count,
                follower_number: tweet.user.followers_count,
                profile_created_dt: tweet.user.created_at,
                profile_image_url: tweet.user.profile_image_url,
                statuses_count: tweet.user.statuses_count,
                rew_tweet_count: tweet.retweet_count,
                lang: tweet.lang,
                source: tweet.source,
                full_name: tweet.user.name,
                location: tweet.user.location,
                favourites: tweet.user.favourites_count
            });
            tweetData.save(function(err, tweetData){
                if(err)
                {
                    //writeFile('Error for ' + tweet.id + ',Error: ' + err + '. at ' + new Date());
                }
            });
        }
    });
}
schedule.scheduleJob({hour: 12, minute: 59}, function(){
    writeFile('Crawler Status report generated at ' + new Date());
    sendreportmail('Crawler Status report at ' + new Date(), 'Crawler Status report at ' + new Date());
});
schedule.scheduleJob({hour: 23, minute: 59}, function(){
    writeFile('Crawler Status report generated and mail sent at ' + new Date());
    sendreportmail('Crawler Status report at ' + new Date(), 'Crawler Status report at ' + new Date());
});

function sendreportmail(subject, text)
{
    var transporter = nodemailer.createTransport(smtpTransport({
        host: 'mail.profilyser.com',
        port: 25,
        auth: {
            user: 'no-reply@profilyser.com',
            pass: 'y%{D`j^@2Rhx3>{j'
        },
        tls: {rejectUnauthorized: false},
        debug: true
    })
            );
    var mailsettings = {
        from: 'Profilyser<no-reply@profilyser.com>',
        to: 'charles.perez@profilyser.com',
        bcc: 'sushoban.manna@dreamztech.com',
        subject: subject,
        text: text,
        attachments: [{path: 'log.txt'}],
        headers: {'reply-to': ''}
    };
    transporter.sendMail(mailsettings, function(error, info){
        if(error){
            writeFile('Sent Mail Error at ' + new Date()+', Error: '+ error);
        }else
        {
            fs.unlinkSync('log.txt');
        }
    });
}
function writeFile(comment)
{
    fd = fs.openSync('log.txt', 'a');
    fs.writeSync(fd, comment + '\n\n');
    fs.closeSync(fd);
}