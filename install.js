var mongoose = require('mongoose');

//connect to local mongodb database
var db = mongoose.connect('mongodb://localhost/sensor');

//attach lister to connected event
mongoose.connection.once('connected', function() {
    console.log("Connected to database")
});
//include model
var user = require('./server/models/user');
var setting = require('./server/models/setting');
var mail = require('./server/models/mailcontent');
var Cms = require('./server/models/cmscontent');
var City = require('./server/models/city');


///insert user
var UserModel = mongoose.model('User');
var userData = new UserModel(
        {
            name: 'Sushobhan Manna',
            twitterId: null,
            email: 'sushobam.manna@dreamztech.com',
            username: 'admin',
            password: '123456',
            roles: ['admin', 'user']
        });
userData.save(function(err, userData) {
    console.log(err);
    console.log("admin successfully inserted.")
});

///insert settings
var SettingModel = mongoose.model('Setting');
var SettingData = [
    {
        key: 'consumer_key',
        label: 'Consumer Key',
        value: 'K3Lr8hC4rs3ExJw7zfj9N671u'
    },
    {
        key: 'consumer_secret',
        label: 'Consumer Secret',
        value: 'hsYvJAYo7KoX1WTvHabZjEPEj1VndxLnKXPH9Qg09ZRk7EQACB'
    },
    {
        key: 'access_token_key',
        label: 'Access Token Key',
        value: '3247033914-ieCFWvxNWWWQ1bJDk5uLhWU5XtG6IJv8GwXBN82'
    },
    {
        key: 'access_token_secret',
        label: 'Access Token Secret',
        value: 'dLzybPwn5GHk8BQhmOc1bBJnuXzQ9FheCLBsk2TPJnZAM'
    },
    {
        key: 'auto_delete_crawler_day',
        label: 'Auto Delete Crawler Day',
        value: '30'
    }
];
SettingModel.collection.insert(SettingData, function() {
    console.log('settings Successfully inserted');
});


///insert mail template
var MailModel = mongoose.model('Mail');
var MailData = [
    {
        name: 'Registration confirmation',
        key: 'register_account',
        subject: 'Your Profilyser account successfully created.',
        template: 'vrehjgbrebgrebgbregugb'
    },
    {
        name: 'forgot_password',
        key: 'forgot_password',
        subject: 'Please reset your password.',
        template: 'jtyjtyjtyjtyjtyjtyj'
    }
];
MailModel.collection.insert(MailData, function() {
    console.log('Mail template Successfully created.');
});


///insert mail template
var CmsModel = mongoose.model('Cms');
var CmsData = [
    {
        name: 'Privacy Policy',
        key: 'privacy_policy',
        content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
    },
    {
        name: 'Terms Of Conditions',
        key: 'terms_of_conditions',
        content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
    }
];
CmsModel.collection.insert(CmsData, function() {
    console.log('Cms template Successfully created.');
});

///insert City template
var CityModel = mongoose.model('City');
var CityData = [
    {
        "name": "Dublin",
        "west_longitude": "-6.44",
        "south_latitude": "53.22",
        "east_longitude": "-6.04",
        "north_latitude": "53.42",
        "consumer_key": "8R9tEzZAtGhh94q8EUlZhXZdw",
        "consumer_secret": "U30bk6XW9Q9ENBUjl8vEpGp8HQLEt9xIvwqSfZZImjfnZnYE83",
        "access_token_key": "3271662728-L7UwX5G3kMniuwdIfd96jGzOzOiWictfmMFv5qz",
        "access_token_secret": "ICfFil8eqsdXLm1mXooStLjOOIGxOrUwBSUjN1XKHm04N",
        "isCrawler": false,
        "allowFreeAccess": false,
        "isDefault": false
    },
    {
        "name": "Paris",
        "west_longitude": "2.22",
        "south_latitude": "48.81",
        "east_longitude": "2.46",
        "north_latitude": "48.90",
        "consumer_key": "31qP287AdYHU1WgPv6upuu4Zq",
        "consumer_secret": "t4PNhIsMkXItV9RfKSAZd50mbqBwsvAvoh1N7Bik3rnMuaUNNj",
        "access_token_key": "3271662728-PAf7ta86I2Pt3OAhzXrb0mdfyDmxf4dj9gXQEID",
        "access_token_secret": "yFCl2f1a82wpCu7gwvFXEfMx2VsHyGyPbI6ebMaKR55sX",
        "isCrawler": true,
        "allowFreeAccess": true,
        "isDefault": false
    },
    {
        "name": "Silicon Valley",
        "west_longitude": "-122.79",
        "south_latitude": "37.14",
        "east_longitude": "-121.56",
        "north_latitude": "38.01",
        "consumer_key": "cWI845CYOS16emyztEFS3u9AI",
        "consumer_secret": "VTyxnx1Rbg9clHDt32qAHjinzL8AQ0DJUxKcPc56sNhjMCAmjt",
        "access_token_key": "3271662728-B2tZGI6P9ovgTUPWe6vy9HlenryqxtAtUstifNJ",
        "access_token_secret": "4ld0ZposXW5sI7WKEaXzNonZSpX2vqw3y1PxCI2X2ujVc",
        "isCrawler": true,
        "allowFreeAccess": true,
        "isDefault": false
    },
    {
        "name": "Ireland",
        "west_longitude": "-10.89",
        "south_latitude": "51.29",
        "east_longitude": "-5.33",
        "north_latitude": "55.34",
        "consumer_key": "cWI845CYOS16emyztEFS3u9AI",
        "consumer_secret": "VTyxnx1Rbg9clHDt32qAHjinzL8AQ0DJUxKcPc56sNhjMCAmjt",
        "access_token_key": "3271662728-B2tZGI6P9ovgTUPWe6vy9HlenryqxtAtUstifNJ",
        "access_token_secret": "4ld0ZposXW5sI7WKEaXzNonZSpX2vqw3y1PxCI2X2ujVc",
        "isCrawler": false,
        "allowFreeAccess": false,
        "isDefault": false
    },
    {
        "name": "London",
        "west_longitude": "-0.66",
        "south_latitude": "51.23",
        "east_longitude": "0.38",
        "north_latitude": "55.34",
        "consumer_key": "vVpKdj9qXc0w1Nk1yHahhKrak",
        "consumer_secret": "TYPoGYtJ8NFIBAilau5II2yX7wbo8M8ZZQXTcDPxz5CdGhPMAs",
        "access_token_key": "3271662728-ZI4JjRq61jLdMmpPX58FsKGZdGtz5xUvYf6LaWL",
        "access_token_secret": "vQI9dOfcLzVEQw1HXUmrFN8wdeEDpIv7ASFCoiu429JF8",
        "isCrawler": false,
        "allowFreeAccess": false,
        "isDefault": false
    },
    {
        "name": "New York",
        "west_longitude": "-74.51",
        "south_latitude": "40.42",
        "east_longitude": "-73.46",
        "north_latitude": "41.08",
        "consumer_key": "vVpKdj9qXc0w1Nk1yHahhKrak",
        "consumer_secret": "TYPoGYtJ8NFIBAilau5II2yX7wbo8M8ZZQXTcDPxz5CdGhPMAs",
        "access_token_key": "3271662728-ZI4JjRq61jLdMmpPX58FsKGZdGtz5xUvYf6LaWL",
        "access_token_secret": "vQI9dOfcLzVEQw1HXUmrFN8wdeEDpIv7ASFCoiu429JF8",
        "isCrawler": true,
        "allowFreeAccess": true,
        "isDefault": false
    },
    {
        "name": "United Kingdom",
        "west_longitude": "-12.75",
        "south_latitude": "49.62",
        "east_longitude": "1.91",
        "north_latitude": "60.15",
        "consumer_key": "ejBoKWBUKeaIZ9zco28foWKE2",
        "consumer_secret": "rTrOoRkOpH6a75HwxeT2LqwtqJETViCO8xZZrAmEnP3SlGkxjm",
        "access_token_key": "3271662728-r74XQNvCkMLLkih7XrtJSoognNEqZAlArOcS0m1",
        "access_token_secret": "D5teUwVEQNK97zGS1jxX3ZwjqawO5z97kSwvrxbVP41Ed",
        "isCrawler": true,
        "allowFreeAccess": true,
        "isDefault": false
    },
    {
        "name": "Champagne-Ardenne (France)",
        "west_longitude": "3.35",
        "south_latitude": "47.90",
        "east_longitude": "4.92",
        "north_latitude": "48.78",
        "consumer_key": "ejBoKWBUKeaIZ9zco28foWKE2",
        "consumer_secret": "rTrOoRkOpH6a75HwxeT2LqwtqJETViCO8xZZrAmEnP3SlGkxjm",
        "access_token_key": "3271662728-r74XQNvCkMLLkih7XrtJSoognNEqZAlArOcS0m1",
        "access_token_secret": "D5teUwVEQNK97zGS1jxX3ZwjqawO5z97kSwvrxbVP41Ed",
        "isCrawler": false,
        "allowFreeAccess": false,
        "isDefault": false
    },
    {
        "name": "Hong-Kong",
        "west_longitude": "113.92",
        "south_latitude": "22.19",
        "east_longitude": "114.32",
        "north_latitude": "22.47",
        "consumer_key": "Fz2CZzL1LRVfCcorYvy4J4JlU",
        "consumer_secret": "rcZpsFXlwFIiME7lEp3Y2Ve24rzO5T7UGJifTd2JAKiBBBU6be",
        "access_token_key": "3271662728-qh8LFwAXXkDwjoPzwDPG7g1skgQdQreYPbc0Lh9",
        "access_token_secret": "vigGdutjz8WHNtaNHAbay2shkSoJI1xq1jaJnm2a4QOzm",
        "isCrawler": false,
        "allowFreeAccess": true,
        "isDefault": true
    },
    {
        "name": "Mayennes",
        "west_longitude": "-1.30",
        "south_latitude": "47.91",
        "east_longitude": "0.05",
        "north_latitude": "48.61",
        "consumer_key": "Fz2CZzL1LRVfCcorYvy4J4JlU",
        "consumer_secret": "rcZpsFXlwFIiME7lEp3Y2Ve24rzO5T7UGJifTd2JAKiBBBU6be",
        "access_token_key": "3271662728-qh8LFwAXXkDwjoPzwDPG7g1skgQdQreYPbc0Lh9",
        "access_token_secret": "vigGdutjz8WHNtaNHAbay2shkSoJI1xq1jaJnm2a4QOzm",
        "isCrawler": false,
        "allowFreeAccess": false,
        "isDefault": false
    }
];
CityModel.collection.insert(CityData, function() {
    console.log('City Inserted Succesfully.');
});
setTimeout(function() {
    process.exit();
}, 6000);