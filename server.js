var express = require('express');
var bodyParser = require('body-parser');
var url = require("url");
var https = require('https');
var http = require('http');
var mongoose = require('mongoose');
var app = express();
var path = require('path');
var robots = require('robots.txt');
// Configuring Passport
var passport = require('passport');
var expressSession = require('express-session');
//var watch = require('node-watch');
var cwd = process.cwd();

//connect to local mongodb database
var db = mongoose.connect('mongodb://localhost/sensor');
//attach lister to connected event
mongoose.connection.once('connected', function () {
    console.log("Connected to database")
});
require('./config/global-constant.js');

//API Router
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// TODO - Why Do we need this key ?
app.use(expressSession({secret: 'profilyser@sushobhan', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
require('./config/twitter-config')(app);

//setup SERVER route
require('./server/routes/routes')(app);


app.get('/*', function (req, res, next) {
    if (req.headers.host.match(/^www/) !== null) {
        res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
    } else {
        next();
    }
})

// Pass in the absolute path to your robots.txt file
app.use(robots(__dirname + '/robots.txt'));
app
        .use(express.static('./public'))
        .get('*', function (req, res) {
            var refererUrl = req.url;
            var urlArr = refererUrl.split("/");
            if (urlArr[1] == 'admin')
            {
                res.sendFile('public/adminLayout.html', {"root": "."});
            }
            else
            {
                res.sendFile('public/index.html', {"root": "."});
            }
        });
var server = http.createServer(app);
server.listen(PORT);
console.log('Server started Suuccessfully at Port ' + PORT);